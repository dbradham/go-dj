const bodyParser = require('body-parser');
const express = require("express");
// const { db } = require('./db');
const fs = require('fs');
const request = require('request');
const SpotifyWebApi = require('spotify-web-api-node');
const nodeSchedule = require('node-schedule');
var async = require('async');
var admin = require('firebase-admin');

var serviceAccount = require("./assets/godj-268901-firebase-adminsdk-driec-319187af4d.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://godj-268901.firebaseio.com"
});
var db = admin.database();
var firestore = admin.firestore();

const spotifyClientId = '272f4705fd4c4d2cb53ff8c16b8cdc3b';
const spotifyClientSecret = '3e2004dfc3fa4479977949fe25bc9b6b';
const redirectUri = 'http://localhost:5000/spotify';

let app = express();

let router = express.Router();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: false })); // support encoded bodies
app.use('/', router);

app.set('views', './client');

app.set('view engine', 'ejs');

router.use(function (req, res, next) {
    const ip = req.ip;
    const log = Date().toString() + ' ' + ip + ' ' 
                + req.method + ' ' + req.path + ' ' + '\n';
    fs.appendFile('logs.log', log,
        (err) => {
            if (err) {
                console.log('failed to update the logs ');
            }
        });
    next();
});

// GET('/djs/create', () => db.djs.create());
// GET('/djs/drop', () => db.djs.drop());
// GET('/hosts/create', () => db.hosts.create());
// GET('/hosts/drop', () => db.hosts.drop());
// GET('/likes/create', () => db.likes.create());
// GET('/likes/drop', () => db.likes.drop());

WEBSITE('/');

SPOTIFY('/spotify');

CURRENTTRACK('/currentTrack');

QUEUE('/queue');

MOBILE('/mobile');

CLIENTCREDENTIALS('/clientcredentials');

WEBCLIENT('/webClient')
/////////////////////////////////////////////
// Express/server part;
/////////////////////////////////////////////
function CLIENTCREDENTIALS(url, handler) {
    try {
        app.post(url, async (req, res) => {
            console.log(req.body);
        });
    } catch (error) {
        res.json({
            success: false,
            error: error || error.message
        });
    }
}

function MOBILE(url, handler) {
    try {
        app.post(url, async (req, res) => {
            const host = req.body.host;
            let db_host = await db.hosts.find(host.email);
            db_host = db_host[db_host.length - 1];
            const accessToken = db_host.token;
            const spotifyApi = new SpotifyWebApi({
                clientId: spotifyClientId,
                clientSecret: spotifyClientSecret,
                redirectUri: redirectUri
            });
            spotifyApi.setAccessToken(accessToken);
            spotifyApi.getMyCurrentPlaybackState({
            }).then((data) => {
                // Output items
                data = data.body;
                res.json({
                    accessToken: accessToken,
                    data: data,
                    host: db_host,
                });
            }, (err) => {
                console.log('Something went wrong!', err);
                res.json({
                    success: false,
                    error: err.message || error,
                });
            });
        });
    } catch(error) {
        res.json({
            success: false,
            error: error.message || error
        })
    }
}
function QUEUE(url, handler) {
    try {
        app.post(url, async (req, res) => {
            let host = req.body.host;
            const song = req.body.song;

            const tasks = [
                () => {
                  updateQueue(host, song);
                },
                () => {
                  scheduleToPlayFromQueue(host, song);
                },
                () => {
                    scheduleDequeue(host, song);
                },    
            ];
            
            async.parallel(tasks, function(err,data) {
              console.log('done doing stuff');
              console.log('data?', data);
              console.log('errors?' ,err);
            });

            res.json({
                success: true,
            });
        })
    } catch (error) {
        res.json({
            success: false,
            error: error.message || error
        });
    }
}

async function updateQueue(host, song) {
    try {
        console.log('host', host);

        firestore
        .collection("hosts")
        .doc(host.id)
        .get()
        .then((user) => {
            const data = user.data();

            console.log('data', data);

            let queue = data.queue;
            queue.push(song);
        
            const newQueueLength = parseInt(data.queueLength) + parseInt(song.songDuration);

            user.ref.update({
                queue: queue,
                queueLength: newQueueLength,
            });
        });
    } catch (error) {
        console.log('error updating queue of host:', host.name, 'error: ', error);
    }
}

async function scheduleToPlayFromQueue(host, song) {
    const accessToken = host.token;
    
    request({
        url: "https://api.spotify.com/v1/me/player/queue?uri=" + song.uri,
        method: "POST",
        json: true,   // <--Very important!!!
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
    }, (error, response, body) => {
        if (error) {
            console.log('error enqueueing to spotify:', error);
        }
    });
}

async function scheduleDequeue(host, song) {
    try {
        const queueLength = parseInt(host.queueLength);
        const songDuration = song.songDuration;
    
        const accessToken = host.token;
        
        const spotifyApi = new SpotifyWebApi({
            clientId: spotifyClientId,
            clientSecret: spotifyClientSecret,
            redirectUri: redirectUri
        });
    
        spotifyApi.setAccessToken(accessToken);
        
        spotifyApi.getMyCurrentPlaybackState({
        }).then((data) => {
            data = data.body;
    
            const duration = data.item.duration_ms;
            const progress = data.progress_ms;
            const remaining = duration - progress;
    
            const date = new Date();
            const nowMs = date.getTime();
    
            if (remaining > 300) {
                const paddingToEnsureWeAreNotEarly = 200;
                const finalMs = remaining + paddingToEnsureWeAreNotEarly + nowMs;
    
                const dateTime = new Date(finalMs + queueLength);
        
                nodeSchedule.scheduleJob(dateTime, () => {
                    dequeue(host, songDuration);
                });
            } else {
                dequeue(host, songDuration);
            }
    
        }, (err) => {
            console.log('error with spotify scheduling dequeue', err);
        });
    } catch (error) {
        error = error.message || error;
        console.log('error with godj scheduling dequeue', error);
    }
}

async function dequeue(host, songDuration) {
    try {
        let dbHost = firestore
            .collection("hosts")
            .doc(host.id);

        let data = await dbHost
            .get()
            .data();

        const queue = data.queue.slice(1);

        const newQueueLength = parseInt(data.queueLength) - songDuration;

        dbHost
        .update({
                queue: queue,
                queueLength: newQueueLength,
            });

        console.log('sucessfully dequeued from: ' + host.name);
    } catch (error) {
        console.log('error dequeueing song from host: ' + host.name, error);
    }
}

async function playFromQueue(uri, host, songDuration) {
    try {
        console.log('playing!!', uri);
        const accessToken = host.token;

        const spotifyApi = new SpotifyWebApi({
            clientId: spotifyClientId,
            clientSecret: spotifyClientSecret,
            redirectUri: redirectUri
        });
        spotifyApi.setAccessToken(accessToken);
        spotifyApi.play({ "uris": [uri] });

        const queue = host.queue;
        const queueLength = parseInt(host.queueLength);
        const newQueueLength = queueLength - songDuration;
        const newQueue = queue.slice(1);

        firestore
        .collection('hosts')
        .doc(host.id)
        .update({
            queue: newQueue,
            queueLength: newQueueLength,
        });
    } catch (error) {
        console.log('error playing song from queue:', error);
    }
}

function WEBCLIENT(url, handler) {
    try {
        app.post(url, async (req, res) => {
            const host = await db.hosts.find(req.body.host.email);
            const accessToken = req.body.accessToken;
            const refreshToken = req.body.refreshToken;
            const spotifyApi = new SpotifyWebApi({
                clientId: spotifyClientId,
                clientSecret: spotifyClientSecret,
                redirectUri: redirectUri
            });
            spotifyApi.setAccessToken(accessToken);
            spotifyApi.setRefreshToken(refreshToken);

            const nowMs = Date.now();
            const oneHourFromNowMs = nowMs + 3600000;
            const oneHourFromNow = new Date(oneHourFromNowMs);
            nodeSchedule.scheduleJob(oneHourFromNow, () => {
                spotifyApi.refreshAccessToken().then((data) => {
                    console.log('refreshed access token');
                    spotifyApi.setAccessToken(data.body['access_token']);
                    db.hosts.updateToken({
                        email: req.body.host.email,
                        accessToken: data.body['access_token']
                    });
                }), (err) => {
                    console.log('could not refresh access token:', err);
                }
            });

            spotifyApi.getMyCurrentPlaybackState({
            }).then((data) => {
                res.json({
                    host: host[host.length - 1],
                    data: data.body
                });
            }, (err) => {
                return err.message || err;
            });
        });
    } catch (error) {
        res.json({
            success: false,
            error: error.message || error,
        });
    }
}

function CURRENTTRACK(url, handler) {
    try {
        app.post(url, async (req, res) => {
            const host = await db.hosts.find(req.body.host.email);
            const accessToken = req.body.accessToken;
            const refreshToken = req.body.refreshToken;
            const spotifyApi = new SpotifyWebApi({
                clientId: spotifyClientId,
                clientSecret: spotifyClientSecret,
                redirectUri: redirectUri
            });
            spotifyApi.setAccessToken(accessToken);
            spotifyApi.setRefreshToken(refreshToken);
            spotifyApi.getMyCurrentPlaybackState({
            }).then((data) => {
                // Output items
                data = data.body;
                res.json({
                    data: data,
                    host: host,
                });
            }, (err) => {
                res.json({
                    success: false,
                    error: err.message || err,
                });
            });
        })
    } catch (error) {
        res.json({
            success: false,
            error: error.message || error,
        });
    }
}

function SPOTIFY(url, handler) {
    app.get(url, async (req, res) => {
        try {
            const authCode = req.query.code;
            const form = {
                code: authCode,
                grant_type: "authorization_code",
                redirect_uri: redirectUri,
            };
            request({
                url: "https://accounts.spotify.com/api/token",
                method: "POST",
                json: true,   // <--Very important!!!
                headers: {
                    'Authorization': 'Basic ' + (new Buffer.from(spotifyClientId + ':' + spotifyClientSecret).toString('base64'))
                },
                form: form
            }, async (error, response, body) => {
                const accessToken = response.body.access_token;
                const refreshToken = response.body.refresh_token;
                res.render('spotify', {
                    accessToken: accessToken,
                    refreshToken: refreshToken,
                });
            });
        } catch (error) {
            res.json({
                success: false,
                error: error.message || error
            });
        }
    })
}

function WEBSITE(url, handler) {
        app.get(url, async (req, res) => {
            try {
                console.log('received get request to access website');
                const authCode = req.query.code;
                console.log('past authCode');
                if (authCode) {
                    const form = {
                        code: authCode,
                        grant_type: "authorization_code",
                        redirect_uri: redirectUri,
                    };
                    console.log('making request to spotify');
                    request({
                        url: "https://accounts.spotify.com/api/token",
                        method: "POST",
                        json: true,   // <--Very important!!!
                        headers: {
                            'Authorization': 'Basic ' + (new Buffer.from(spotifyClientId + ':' + spotifyClientSecret).toString('base64'))
                        },
                        form: form
                    }, async (error, response, body) => {
                        const accessToken = response.body.access_token;
                        const refreshToken = response.body.refresh_token;
                        res.render('home', {
                            accessToken: accessToken,
                            refreshToken: refreshToken,
                        });
                    });
                }
                else {
                    console.log('rendering sign in page');
                    res.render('signin');
                }
            } catch (error) {
        res.json({
            success: false,
            error: error.message || error
        });
    }
});
}

function GET(url, handler) {
    app.get(url, async (req, res) => {
        try {
            const data = await handler(req);
            res.json({
                success: true,
                data
            });
        } catch (error) {
            res.json({
                success: false,
                error: error.message || error
            });
        }
    });
}

app.get('/spotify-login', (req, res) => {
    const my_client_id = '272f4705fd4c4d2cb53ff8c16b8cdc3b';
    const scopes = 'user-read-currently-playing user-modify-playback-state user-read-playback-state user-read-private user-read-email streaming app-remote-control';
    res.redirect('https://accounts.spotify.com/authorize' +
        '?response_type=code' +
        '&client_id=' + my_client_id +
        (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
        '&redirect_uri=' + encodeURIComponent(redirectUri));
});

app.use(express.static('client'));

const port = 8080;

app.listen(port, () => {
    console.log('\nReady for POST requests on http://localhost:' + port);
});