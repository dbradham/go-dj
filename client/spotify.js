import ReactDOM from 'react-dom';
import React from 'react';
import SpotifyControl from './components/SpotifyControl';

const spotifyWrapper = document.getElementById('spotify-wrapper');
ReactDOM.render(<SpotifyControl />, spotifyWrapper);