import ReactDOM from 'react-dom';
import React from 'react';
import Signin from './components/Signin';

const signinWrapper = document.getElementById('signin-wrapper');
ReactDOM.render(<Signin />, signinWrapper);