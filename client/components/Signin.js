import React, { Component } from "react";
import Signup from './Signup';
import Login from './Login';
import SpotifyControl from './SpotifyControl';
import Home from './Home';
import Header from './Header';
import firebase from '../../firebase';

export default class Signin extends Component {
    constructor() {
        super();

        this.state = {
            screen: '',
            email: '',
            bio: '',
            bio2: '',
            name: '',
            location: '',
            user: null,
        }
        this.back = this.back.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.signup = this.signup.bind(this);
        this.login = this.login.bind(this);
        this.signout = this.signout.bind(this);
    }

    componentDidMount() {
        this.checkIfAuthenticated();
      }
    
      checkIfAuthenticated() {
        firebase.auth().onAuthStateChanged((user) => {
          if (user != null) {
            this.setState({ 
              screen: 'options', 
              user: user 
            });
          } else {
            this.setState({ screen: 'login' });
          }
        });
      }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }
    
    signout() {
        try {
          firebase.auth().signOut();
          this.setState({
              screen: 'login',
              user: null,
          });
        } catch (error) {
          console.log('error signing out', error.message || error);
        }
    }

    signup() {
        try {
          const email = this.state.email;
          const bio = this.state.bio;
          firebase.auth().createUserWithEmailAndPassword(email, bio);
          firebase.auth().onAuthStateChanged(user => {
            if (user == null) return;

            const updateFirestore = (uid, email) => {
              const users = firebase.firestore().collection('users');
              async function add(id, email) {
                await users.add({
                  uid: uid,
                  email: email
                });
              }
              add(uid, email);
            }
    
            updateFirestore(user.uid, user.email);
            this.setState({
                screen: 'options',
                user: {
                    uid: user.uid,
                    email: user.email,
                }
            });
          });
        } catch (error) {
          console.log(error.toString(error));
        }
    }

    login() {
        const email = this.state.email;
        const bio = this.state.bio;
        try {
          firebase.auth().signInWithEmailAndPassword(email, bio);
          this.setDbUser(email);
        } catch (error) {
          console.log({
            success: false,
            error: error.message || error
          });
        }
    }

    setDbUser(email) {
        firebase
        .firestore()
        .collection('users')
        .where('email', '==', email)
        .onSnapshot((snapshot) => {
          // todo: fix this so it's not mapping, 
          // just grab the first element of the array
          const user = snapshot.docs[0];
          this.setState({ 
            user: user,
            screen: 'options',
          });
        });
      }

    back(screen = '') {
        this.setState({ screen: screen });
    }

    render() {
        if (this.state.screen == 'signup') {
            return <div id="signin-container">
                <Header />
                <Signup handleChange={this.handleChange}
                    signup={this.signup} back={this.back} />
            </div>
        } else if (this.state.screen == 'spotify') {
            return <SpotifyControl host={this.state.host} />;
        } else if (this.state.screen == 'options') {
            return <Home user={this.state.user}
            signout={this.signout} />;
        } else {
            return <div id="signin-container" className="container">
                <Header />
                <Login handleChange={this.handleChange} 
                    login={this.login} back={this.back} />
            </div>;
        }
    }
}