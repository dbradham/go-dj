import React from "react";
import PropTypes from "prop-types";

const BaseButton = (props) => {
        return <button
                onClick={props.onClick}
                className={props.className}
                type={props.type}
                id={props.id}
            >{props.symbol}{props.text}
            </button>
}

BaseButton.propTypes = {
    type: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
};
export default BaseButton;