import React, { Component } from "react";
import BaseButton from './BaseButton';
import BaseTextInput from './BaseTextInput';

const Signup = (props) => {
        return (
            <div id="signup-container" className="container">
                <div id="form">
                    <div id="form-box">
                        <BaseTextInput
                            name="email"
                            text="Email"
                            label="email"
                            id="email"
                            className="form-input"
                            handleChange={props.handleChange}
                        /><BaseTextInput
                            hidden={true}
                            name="bio"
                            text="Password"
                            label="bio"
                            id="bio"
                            className="form-input"
                            handleChange={props.handleChange}
                        /><BaseTextInput
                            hidden={true}
                            name="bio2"
                            text="Confirm Password"
                            label="bio2"
                            id="bio2"
                            className="form-input"
                            handleChange={props.handleChange}
                        />
                        <BaseButton
                            text="Sign Up"
                            type="button"
                            id="signup-submit"
                            className="submit btn"
                            onClick={props.signup}
                        /><BaseButton
                            text="Cancel"
                            type="button"
                            id="cancel-btn"
                            className="cancel-btn btn"
                            onClick={props.back}
                        />
                    </div>
                </div>
            </div>
        );
}
export default Signup;