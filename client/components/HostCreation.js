import React from "react";
import BaseButton from './BaseButton';
import BaseTextInput from './BaseTextInput';
import Header from './Header';

const HostCreation = (props) => {
        return (
            <div id="host-wrapper" className="container">
                <Header signout={props.signout} />
                <div id="host-form">
                    <div id="form-box">
                        <BaseTextInput
                            name="name"
                            text="Venue Name"
                            label="name"
                            id="name"
                            className="form-input"
                            handleChange={props.handleChange}
                        /><BaseTextInput
                            name="location"
                            text="Location"
                            label="location"
                            id="location"
                            className="form-input"
                            handleChange={props.handleChange}
                        />
                        <BaseButton
                            text="Create Venue"
                            type="button"
                            id="signup-submit"
                            className="submit btn"
                            onClick={() => props.createHost()}
                        /><BaseButton
                            text="Cancel"
                            type="button"
                            id="cancel-btn"
                            className="cancel-btn btn"
                            onClick={() => props.back()}
                        />
                    </div>
                </div>
            </div>
        );
}
export default HostCreation;