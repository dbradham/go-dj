import React, { Component } from 'react';

export default class HomeInfo extends Component {
    constructor() {
        super();

        this.state = {
            slide: 1,
        }
    }

    readSlide() {
        let slide = null;
        switch (this.state.slide) {
            case 1:
                slide = <div className="slide-pic"
                    id="guests-slide">
                    <p>GoDJ lets guests control what is playing</p>
                </div>
                break
            case 2:
                slide = <div className="slide-pic"
                    id="aux-slide">
                    <p>GoDJ means no more aux cord distractions</p>
                </div>
                break
            case 3:
                slide = <div className="slide-pic"
                    id="lines-slide">
                    <p>GoDJ means no more long lines</p>
                </div>
                break
        }
        return slide;
    }

    render() {
        const slide = this.readSlide();
        return <div id="slides">
            {slide}
            <div id="dots">
                <a onClick={() => this.setState({ slide: 1 })} 
                    href="javascript:void(0)" className="dot">.</a>
                <a onClick={() => this.setState({ slide: 2 })} 
                    href="javascript:void(0)" className="dot">.</a>
                <a onClick={() => this.setState({ slide: 3 })} 
                    href="javascript:void(0)" className="dot">.</a>
            </div>
        </div>
    }
}