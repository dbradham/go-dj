import React from 'react';

const Options = () => {
    return <div id="main-wrapper">
        <h3 id="subheader">Choose your Stream</h3>
        <div className="options-container">
            <div className="option">
                <img src="../images/spotify.png" alt="Spotify Logo"
                    id="spotify-img" className="option-img"></img>
                <a href="/spotify-login"
                    id="spotify-text"
                    className="option-text">Spotify</a>
            </div>
            <div className="option">
                <img src="../images/apple.png" alt="Apple Music Logo"
                    id="apple-img" className="option-img"></img>
                <a id="apple-text" className="option-text">Coming soon...</a>
            </div>
        </div>
    </div>
}
export default Options;