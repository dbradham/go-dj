import React from 'react';

const Search = (props) => {
        return  <div id="search-container">
            <input
                id="search"
                placeholder="Search products..."
                onChange={props.handleChange}
                type="text"
                onKeyDown={(e) => props.search(e.key)} />
                <button id="search-button" onClick={props.search('enter')}
                >Search</button>
        </div>
}

export default Search;