import React from 'react';

const ListedSong = (props) => {
    const className = "listed-song " + props.className;
    const albumCaption = props.album + ' cover';
    let title = props.title.split('(')[0];
    title = title.length >= 23 ? title.substring(0, 22) + '...' : title;
    let album = props.album.split('(')[0];
    album = album.length >= 23 ? album.substring(0, 22) + '...' : album;
    return <div className={className}>
        <span>{props.requestedBy}</span>
        <span className="queue-song-name">{title}</span>
        <img src={props.src} alt={albumCaption}
            className="queue-album-cover"></img>
        <span className="queue-artist">{props.artist}</span>
        <span className="queue-album-name">{album}</span>
    </div>
}

export default ListedSong;