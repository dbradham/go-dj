import React from 'react';

const Header = (props) => {
    const signout = props.signout ? <a onClick={() => props.signout()}
    href="#" className="header-smol signout">Signout</a> 
    :
    null;
    return <div id="header">
        <a href="#" id="header-title">Go DJ</a>
        {/* <a className="header-smol">About</a>
        <a className="header-smol">Venues</a>
        <a className="header-smol">DJs</a> */}
        <a className="header-smol"></a>
        {signout}
    </div>  
}

export default Header;
