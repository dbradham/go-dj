import React from "react";
import { PropTypes } from "prop-types";

const BaseTextInput = (props) => {
    const type = props.hidden == true ? 'password' : 'text';
    
    return (
        <div className={props.className}>
            <label htmlFor={props.label}>{props.text}</label>
            <input
                type={type}
                name={props.name}
                className="form-control"
                id={props.id}
                value={props.value}
                onChange={props.handleChange}
                placeholder={props.placeholder}
            />
        </div>
    )   
}
BaseTextInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
};

export default BaseTextInput;