import React from 'react';
import Options from './Options';
import Header from './Header';
import HomeInfo from './HomeInfo';

const Home = (props) => {
    return <div id="home">
        <Header user={props.user} signout={props.signout} />
        <Options />
    </div>
}
export default Home;