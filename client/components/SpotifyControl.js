import React, { Component } from 'react';
import Home from './Home';
import ListedSong from './ListedSong';
import Header from './Header';
import firebase from '../../firebase';
import SignIn from './Signin';
import HostCreation from './HostCreation';

export default class SpotifyControl extends Component {
    constructor() {
        super();

        this.state = {
            host: {
                queue: ''
            },
            search: '',
            song: null,
            songName: '',
            images: '',
            album: '',
            artist: '',
            device: '',
            type: '',
            hasData: false,
            screen: '',
            user: null,
            name: '',
            location: '',
            accessToken: '',
        }
        this.search = this.search.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.play = this.play.bind(this);
        this.selectHost = this.selectHost.bind(this);
        this.getNextSong = this.getNextSong.bind(this);
        this.signout = this.signout.bind(this);
        this.createHost = this.createHost.bind(this);
        this.back = this.back.bind(this);
    }

    componentDidMount() {
        const accessToken = document.getElementById('access-token').innerText;
        const refreshToken = document.getElementById('refresh-token').innerText;
        this.checkIfAuthenticated(accessToken);
    }

    checkIfAuthenticated(accessToken) {
        firebase.auth().onAuthStateChanged((user) => {
          if (user != null) {
            this.setAccessToken(accessToken, user);
          } else {
            this.setState({
                accessToken: accessToken,
                screen: 'signin',
                user: null,
            });
          }
        });
      }

    getCurrentSong(host, user, accessToken) {
        const Spotify = require('spotify-web-api-js');
        let spotifyApi = new Spotify();
        spotifyApi.setAccessToken(accessToken);
        spotifyApi.getMyCurrentPlayingTrack((err, data) => {
            if (err) {
                console.error(err);
            }
            else {
                this.setState({ 
                    accessToken: accessToken,
                    song: data.item,
                    hasData: true,
                    user: user,
                    host: host,
                });
            }
        });
    }

    setAccessToken(accessToken, user) {
        firebase
        .firestore()
        .collection('hosts')
        .doc(user.uid)
        .get()
        .then(doc => {
            console.log('doc', doc.data());
            let host = doc.data();

            if (host.email) {
                host.update({ token: accessToken });
    
                host.token = accessToken;
            }
            this.getCurrentSong(host, user, accessToken);
        });
    }

    selectHost(host) {
        const accessToken = document.getElementById('access-token').innerText;
        const refreshToken = document.getElementById('refresh-token').innerText;
        const uri = '/webClient';
        fetch(uri, {
            method: "post",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                accessToken: accessToken,
                refreshToken: refreshToken,
                host: host
            })
        })
            .then((res) => res.json())
            .then((result) => {
                const data = result.data;
                if (this.state.screen != 'options') {
                    this.setState({
                        // TODO- song: result.data.item
                        song: data,
                        songName: data.item.name,
                        album: data.item.album.name,
                        artist: data.item.artists[0].name,
                        images: data.item.album.images,
                        device: data.device.name,
                        type: data.device.type,
                        host: result.host,
                        hasData: true,
                    });
                    this.getNextSong(result.host);
                }
            })
            .catch((error) => {
                console.log('failed to load current track');
                console.log(error);
            });
    }

    getNextSong(host) {
        const remaining = parseInt(this.state.song.item.duration_ms) - parseInt(this.state.song.progress_ms);
        const interval = remaining < 1500 ? remaining : 1500;
        setTimeout(() => {
            if (this.state.host.name == '' || this.state.screen == 'options') {
                return;
            } else {
                this.selectHost(host);
            }
        }, interval);
    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    play() {
        const accessToken = document.getElementById('access-token').innerText;
        const body = JSON.stringify({ "uris": ["spotify:track:2B0AxMqUogPFVVU5smNpTo"] });
        const songs = { "uris": ["spotify:track:3u9u4xAkzKuONeP4QD9E3q", "spotify:track:2B0AxMqUogPFVVU5smNpTo"] }
        const uri = 'https://api.spotify.com/v1/me/player/play';
        fetch(uri, {
            method: "put",
            headers: {
                'Authorization': 'Bearer ' + accessToken
            },
            body: body
        })
            .then((res) => res.json())
            .then((result) => {

            })
            .catch((error) => {
                console.log('failed to play track');
                console.log(error);
            });
    }

    search(key) {
        if (key === 'Enter') {
            const keyword = this.state.search;
            let displayData = [];
            console.log(keyword);
            // this.state.data.map((item) => {
            //     if (item.name.includes(keyword)) {
            //         displayData.push(item);
            //     }
            // });
            // console.log(displayData);
            // this.setState({ displayData: displayData });
        }
    }

    readQueue(queue) {
        if (queue == undefined || queue.length == 0){
            return null;
        }
        let count = 0;
        console.log('queue', queue);
        const songs = queue.map((song) => {
            if (song == '') {
                return null;
            }
            count += 1;
            let title = song.name;
            let artist = song.artists[0].name;
            let album = song.album.name;
            let img = song.album.images[0].url;
            let uri = song.uri;
            return <ListedSong  
                title={title}
                src={img}
                album={album}
                artist={artist}
                uri={uri}
                className="queue-song"
                requestedBy={song.requestedBy} />
        });
        console.log(songs);
        return <div id="queue">
            {songs}
        </div>
    }

    signout() {
        try {
          firebase.auth().signOut();
          this.setState({
              screen: 'signin',
              user: null,
          });
        } catch (error) {
          console.log('error signing out', error.message || error);
        }
    }

    createHost() {
        const name = this.state.name;
        const location = this.state.location;
        const user = this.state.user;

        // this.uploadImage(imageUri, name)
        //     .then(() => {
        //         console.log('success uploading image');
        //     })
        //     .catch((error) => {
        //         console.log('error:', error);
        //     });
        
        const host = {
            name: name,
            location: location,
            public: true,
            visible: true,
            patrons: [],
            queue: [],
        }

        firebase
        .firestore()
        .collection("hosts")
        .doc(user.uid)
        .set(host)
        .then(function() {
            const user = this.state.user;
            const accessToken = this.state.accessToken;
            this.getCurrentSong(host, user, accessToken);
        }.bind(this))
        .catch(function(error) {
            console.error("Error writing document: ", error);
        });
    }

    back() {
        this.setState({
            screen: 'options'
        });
    }

    render() {
        console.log('screen', this.state.screen);
        if (this.state.screen == 'signin') {
            return <SignIn />
        } else if (this.state.screen == 'host creation') {
            return <HostCreation createHost={this.createHost} 
                    handleChange={this.handleChange} 
                    back={this.back} 
                    signout={this.signout}/>;
        }

        let queueContainer = '';
        if (this.state.host.queue != '') {
            console.log(this.state.host);
            const queue = this.readQueue(this.state.host.queue);
            console.log('queue', queue);
            queueContainer = <div id="spotify-queue">
                {queue}
            </div>
        }
        const song = this.state.song;

        if (song) {
            const images = song.album.images;
            const name = song.name.split('(')[0];
            const album = song.album.name.split('(')[0];
            const artist = song.artist;

            //const type = song.type;
            //const device = song.device;            
            const device = '';
            const type = '';

            const alt = 'album cover of: ' + album;
            const img = images[0].url;

            return <div id="host-wrapper">
                <Header signout={this.signout} />
                    <div id="now-playing">
                        <div id="song-container">
                            <span id="song-name">{name}</span>
                            <span id="artist-name">{artist}</span>
                            <span id="album-name">{album}</span>
                        </div>
                        <img src={img} alt={alt} id="album-cover"></img>
                        {/* add some play/pause, skip, and rewind buttons */}
                        <div id="device-container">
                            <span id="device-name">{device} - {type}</span>
                        </div>
                    </div>
                    {queueContainer}
            </div>
        } else if (this.state.screen == 'options') {
            return <Home />;
        } else if (!this.state.hasData) {
            return <div id="no-data"><img src="../images/loading.gif" alt="loading icon"></img></div>
        } else {
            return <div id="spotify-control-container">
                <p>No song currently playing...</p>
            </div>
        }
        // const img = images[0].url;
        // console.log('img', img)
    }
}