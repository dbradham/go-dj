import React from "react";
import BaseButton from './BaseButton';
import BaseTextInput from './BaseTextInput';

const Login = (props) => {
    return <div id="login-container" className="container">
        <div id="form">
            <div id="form-box">
                <h1 id="signin-text">Sign in to GoDJ</h1>
                <BaseTextInput
                    name="email"
                    text="Email"
                    label="email"
                    id="email"
                    className="form-input"
                    handleChange={props.handleChange}/>
                <BaseTextInput
                    hidden={true}
                    name="bio"
                    text="Password"
                    label="bio"
                    id="bio"
                    className="form-input"
                    handleChange={props.handleChange}/>
                <div id="buttons">
                    <div id="login-btn-container">
                        <BaseButton
                            text="Login"
                            type="button"
                            id="login-submit"
                            className="submit btn"
                            onClick={() => props.login()}/>
                    </div>
                    <div id="lower-form-box">
                        <BaseButton
                            text="Create a New Account"
                            type="button"
                            id="signup-btn"
                            className="signup-btn btn"
                            onClick={() => props.back("signup")}/>
                    </div>
                </div>
            </div>
        </div>
    </div>
}
export default Login;