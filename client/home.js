import ReactDOM from 'react-dom';
import React from 'react';
import Home from './components/Home';

const optionsWrapper = document.getElementById('content-flexbox');
ReactDOM.render(<Home />, optionsWrapper);