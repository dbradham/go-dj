const sql = require('../sql').products;

const cs = {}; // Reusable ColumnSet objects.

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */

class ProductsRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        createColumnsets(pgp);
    }

    // Creates the table;
    async create() {
        return this.db.none(sql.create);
    }

    async addSalePrice() {
        return this.db.none(sql.addSalePrice);
    }
    async addSizes() {
        return this.db.none(sql.addSizes);
    }
    async alterSizes() {
        return this.db.none(sql.alterSizes);
    }
    // Drops the table;
    async drop() {
        return this.db.none(sql.drop);
    }

    // Removes all records from the table;
    async empty() {
        return this.db.none(sql.empty);
    }

    async gender() {
        this.db.any(sql.gender);
        return 'success';
    }

    // Adds a new record and returns the full object;
    // It is also an example of mapping HTTP requests into query parameters;
    async add(values) {
        try {
            this.db.none(sql.add, {
                name: values.name,
                price: parseInt(values.price),
                discriminator: values.discriminator,
                type: values.typeSelector,
                gender: values.gender,
                image: values.image,
                small: values.small,
                medium: values.medium,
                large: values.large,
            });
            return 'success';
        } catch(e) {
            console.log('error');
            console.log(e);
            return e;
        }
    }

    // Tries to delete a product by id, and returns the number of records deleted;
    async remove(id) {
        return this.db.result('DELETE FROM products WHERE id = $1', +id, r => r.rowCount);
    }

    // Tries to find a user product from user id + product name;
    async find(values) {
        return this.db.oneOrNone(sql.find, {
            productName: values.name
        });
    }

    // Returns all product records;
    async all() {
        return this.db.any('SELECT * FROM products');
    }

    // Returns the total number of products;
    async total() {
        return this.db.one('SELECT count(*) FROM products', [], a => +a.count);
    }

    async by_discriminator(discriminator) {
        return this.db.any(sql.by_discriminator, {
            discriminator: discriminator
        })
    }

    async by_discriminator_and_gender(values) {
        return this.db.any(sql.by_discriminator_and_gender, {
            discriminator: values.discriminator,
            gender: values.gender
        });
    }
    async by_discriminator_type_and_gender(values) {
        return this.db.any(sql.by_discriminator_type_and_gender, {
            discriminator: values.discriminator,
            gender: values.gender,
            type: values.type
        });
    }
    async by_discriminator_and_type(values) {
        return this.db.any(sql.by_discriminator_and_type, {
            discriminator: values.discriminator,
            type: values.type
        });
    }
    async sales() {
        return this.db.any(sql.sales);
    }
    async sales_by_gender(gender) {
        return this.db.any(sql.sales_by_gender, {
            gender: gender
        });
    }
    async sales_by_gender_discriminator(values) {
        return this.db.any(sql.sales_by_gender_discriminator, {
            gender: values.gender,
            discriminator: values.discriminator
        });
    }
    async sales_by_gender_discriminator_type(values) {
        return this.db.any(sql.sales_by_gender_discriminator_type, {
            gender: values.gender,
            discriminator: values.discriminator,
            type: values.type
        });    
    }
    async sales_by_discriminator_type(values) {
        return this.db.any(sql.sales_by_discriminator_type, {
            discriminator: values.discriminator,
            type: values.type
        });
    }
    async search(product) {
        const productString = '%' + product + '%';
        return this.db.any(sql.search, {
            product: productString
        });
    }

    async updateInventory(values) {
        return this.db.any(sql.updateInventory, {
            id: values.id,
            large: values.large,
            medium: values.medium,
            small: values.small
        })
    }
}

//////////////////////////////////////////////////////////
// Example of statically initializing ColumnSet objects:

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs.insert) {
        // Type TableName is useful when schema isn't default "public" ,
        // otherwise you can just pass in a string for the table name.
        const table = new pgp.helpers.TableName({ table: 'products', schema: 'public' });

        cs.insert = new pgp.helpers.ColumnSet(['name'], { table });
        cs.update = cs.insert.extend(['?id', '?user_id']);
    }
    return cs;
}

module.exports = ProductsRepository;