const sql = require('../sql').djs;

const cs = {}; // Reusable ColumnSet objects.

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */


class DjsRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        createColumnsets(pgp);
    }

    // Creates the table;
    async create() {
        return this.db.none(sql.create);
    }
    async drop() {
        return this.db.none("DROP TABLE djs");
    }

    async add(values) {
        this.db.none(sql.add, {
            email: values.email,
            password: values.subject,
        });
        return 'success';
    }

    async find(email) {
        try {
            return this.db.any(sql.find, {
                email: email
            });
        } catch(error) {
            console.log(error);
            console.log(error.message);
        }
    }

    async remove(values) {
        this.db.none(sql.remove, {
            userId: values.userId,
            productId: values.productId,
            content: values.content
        });
        return 'success';
    }

    // Returns all product records;
    async all() {
        return this.db.any('SELECT * FROM djs');
    }

    // Returns the total number of products;
    async total() {
        return this.db.one('SELECT count(*) FROM djs', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// Example of statically initializing ColumnSet objects:

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs.insert) {
        // Type TableName is useful when schema isn't default "public" ,
        // otherwise you can just pass in a string for the table name.
        const table = new pgp.helpers.TableName({ table: 'djs', schema: 'public' });

        cs.insert = new pgp.helpers.ColumnSet(['name'], { table });
    }
    return cs;
}

module.exports = DjsRepository;