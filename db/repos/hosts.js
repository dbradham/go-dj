const sql = require('../sql').hosts;

const cs = {}; // Reusable ColumnSet objects.

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */


class HostsRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        createColumnsets(pgp);
    }

    // Creates the table;
    async create() {
        return this.db.none(sql.create);
    }
    async drop() {
        return this.db.none(sql.drop);
    }

    async add(values) {
        this.db.none(sql.add, {
            token: values.accessToken,
            name: values.name,
            location: values.location,
            email: values.email,
            password: values.subject
        });
        return 'success';
    }

    async updateToken(values) {
        return this.db.result(sql.updateToken, {
            email: values.email,
            token: values.accessToken
        })
    }

    async login(email) {
        return this.db.result(sql.login, {
            email: email,
        })
    }

    async signout(email) {
        return this.db.result(sql.signout, {
            email: email,
        })
    }

    async find(email) {
        try {
            return this.db.any(sql.find, {
                email: email
            });
        } catch (error) {
            console.log(error);
            return {
                success: 'false'
            }
        }
    }

    async queue(values) {
        try {
            return this.db.result(sql.queue, {
                email: values.email,
                queue: values.queue,
                queueLength: values.queueLength
            });
        } catch (error) {
            console.log(error);
            return {
                success: false
            }
        }
    }

    async available() {
        try {
            return this.db.any(sql.available);
        } catch (error) {
            console.log(error);
            return {
                success: false
            }
        }
    }

    async remove(values) {
        this.db.none(sql.remove, {
            userId: values.userId,
            productId: values.productId,
            content: values.content
        });
        return 'success';
    }

    // Returns all product records;
    async all() {
        return this.db.any('SELECT * FROM hosts');
    }

    // Returns the total number of products;
    async total() {
        return this.db.one('SELECT count(*) FROM hosts', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// Example of statically initializing ColumnSet objects:

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs.insert) {
        // Type TableName is useful when schema isn't default "public" ,
        // otherwise you can just pass in a string for the table name.
        const table = new pgp.helpers.TableName({ table: 'hosts', schema: 'public' });

        cs.insert = new pgp.helpers.ColumnSet(['name'], { table });
    }
    return cs;
}

module.exports = HostsRepository;