const sql = require('../sql').reviews;

const cs = {}; // Reusable ColumnSet objects.

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */


class ReviewsRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        createColumnsets(pgp);
    }

    // Creates the table;
    async create() {
        return this.db.none(sql.create);
    }
    async drop() {
        return this.db.none(sql.drop);
    }

    async add(values) {
        this.db.none(sql.add, {
            userId: values.userId,
            productId: values.productId,
            content: values.content
        });
        return 'success';
    }

    async remove(values) {
        this.db.none(sql.remove, {
            userId: values.userId,
            productId: values.productId,
            content: values.content
        });
        return 'success';
    }

    async product(values) {
        return this.db.any(sql.product, {
            productId: values.productId
        });
    }

    // Returns all product records;
    async all() {
        return this.db.any('SELECT * FROM reviews');
    }

    // Returns the total number of products;
    async total() {
        return this.db.one('SELECT count(*) FROM reviews', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// Example of statically initializing ColumnSet objects:

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs.insert) {
        // Type TableName is useful when schema isn't default "public" ,
        // otherwise you can just pass in a string for the table name.
        const table = new pgp.helpers.TableName({ table: 'reviews', schema: 'public' });

        cs.insert = new pgp.helpers.ColumnSet(['name'], { table });
        cs.update = cs.insert.extend(['?id', '?user_id']);
    }
    return cs;
}

module.exports = ReviewsRepository;