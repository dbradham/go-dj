const sql = require('../sql').likes;

const cs = {}; // Reusable ColumnSet objects.

/*
 This repository mixes hard-coded and dynamic SQL, primarily to show a diverse example of using both.
 */


class LikesRepository {
    constructor(db, pgp) {
        this.db = db;
        this.pgp = pgp;

        // set-up all ColumnSet objects, if needed:
        createColumnsets(pgp);
    }

    // Creates the table;
    async create() {
        return this.db.none(sql.create);
    }
    async drop() {
        return this.db.none(sql.drop);
    }

    async add(values) {
        return this.db.result(sql.add, {
            email: values.email,
            id: values.id
        });
    }

    async remove(values) {
        this.db.none(sql.remove, {
            email: values.email,
            id: values.id
        });
        return 'success';
    }

    async find(email) {
        return this.db.any(sql.find, {
            email: email
        });
    }

    // Returns all product records;
    async all() {
        return this.db.any('SELECT * FROM likes');
    }

    // Returns the total number of products;
    async total() {
        return this.db.one('SELECT count(*) FROM likes', [], a => +a.count);
    }
}

//////////////////////////////////////////////////////////
// Example of statically initializing ColumnSet objects:

function createColumnsets(pgp) {
    // create all ColumnSet objects only once:
    if (!cs.insert) {
        // Type TableName is useful when schema isn't default "public" ,
        // otherwise you can just pass in a string for the table name.
        const table = new pgp.helpers.TableName({ table: 'likes', schema: 'public' });

        cs.insert = new pgp.helpers.ColumnSet(['name'], { table });
    }
    return cs;
}

module.exports = LikesRepository;