/*
    Inserts a new Host record.
*/
INSERT INTO hosts(email, password, name, visibility, location, token, queue, queueLength)
VALUES(${email}, ${password}, ${name}, false, ${location}, ${token}, '', 0);