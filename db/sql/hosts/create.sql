CREATE TABLE hosts
(
    email text not null,
    password text not null,
    name text not NULL,
    location text not NULL,
    visibility Boolean,
    token text,
    queue text,
    queueLength int not null
)