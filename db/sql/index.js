const { QueryFile } = require('pg-promise');
const path = require('path');

///////////////////////////////////////////////////////////////////////////////////////////////
// Criteria for deciding whether to place a particular query into an external SQL file or to
// keep it in-line (hard-coded):
//
// - Size / complexity of the query, because having it in a separate file will let you develop
//   the query and see the immediate updates without having to restart your application.
//
// - The necessity to document your query, and possibly keeping its multiple versions commented
//   out in the query file.
//
// In fact, the only reason one might want to keep a query in-line within the code is to be able
// to easily see the relation between the query logic and its formatting parameters. However, this
// is very easy to overcome by using only Named Parameters for your query formatting.
////////////////////////////////////////////////////////////////////////////////////////////////

module.exports = {
    djs: {
        create: sql('djs/create.sql'),
        add: sql('djs/add.sql'),
        remove: sql('djs/remove.sql'),
        find: sql('djs/find.sql'),
    },
    hosts: {
        create: sql('hosts/create.sql'),
        add: sql('hosts/add.sql'),
        remove: sql('hosts/remove.sql'),
        drop: sql('hosts/drop.sql'),
        find: sql('hosts/find.sql'),
        queue: sql('/hosts/queue.sql'),
        available: sql('hosts/available.sql'),
        updateToken: sql('hosts/updateToken.sql'),
        login: sql('hosts/login.sql'),
        signout: sql('hosts/signout.sql'),
    },
    likes: {
        create: sql('likes/create.sql'),
        add: sql('likes/add.sql'),
        remove: sql('likes/remove.sql'),
        find: sql('likes/find.sql'),
        drop: sql('likes/drop.sql'),
    }
};

///////////////////////////////////////////////
// Helper for linking to external query files;
function sql(file) {

    const fullPath = path.join(__dirname, file); // generating full path;

    const options = {

        // minifying the SQL is always advised;
        // see also option 'compress' in the API;
        minify: true

        // See also property 'params' for two-step template formatting
    };

    const qf = new QueryFile(fullPath, options);

    if (qf.error) {
        // Something is wrong with our query file :(
        // Testing all files through queries can be cumbersome,
        // so we also report it here, while loading the module:
        console.error(qf.error);
    }

    return qf;

    // See QueryFile API:
    // http://vitaly-t.github.io/pg-promise/QueryFile.html
}

///////////////////////////////////////////////////////////////////
// Possible alternative - enumerating all SQL files automatically:
// http://vitaly-t.github.io/pg-promise/utils.html#.enumSql