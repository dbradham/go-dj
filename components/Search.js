import { Item, Form, Input, Button, Label } from "native-base";
import React, { Component } from 'react';
import { Ionicons } from "@expo/vector-icons";
import SongChoice from './SongChoice';
import {
    Modal,
    View,
    StyleSheet,
    Dimensions,
    ScrollView,
    ActivityIndicator,
    Platform
} from 'react-native';

export default class Search extends Component {
    constructor() {
        super();

        this.state = {
            modalVisible: false,
            searchText: '',
            searchResult: null,
            loading: false,
            dj: {},
        };
    }

    componentDidMount() {
        this.setState({
            dj: this.props.dj,
            host: this.props.host,
        });
    }

    searchSpotify() {
        this.setState({ loading: true, modalVisible: true });
        const likedSongs = this.props.likedSongs.map((song) => {return song.name + song.artists[0].name});
        const input = this.state.searchText;
        const Spotify = require('spotify-web-api-js');
        let spotifyApi = new Spotify();
        result = [];
        spotifyApi.setAccessToken(this.state.host.token);
        // search tracks whose name, album or artist contains 'Love'
        spotifyApi.searchTracks(input)
            .then(function (data) {
                for (let key in data.tracks.items) {
                    let song = data.tracks.items[key];
                    if (likedSongs.includes(song.name + song.artists[0].name)) {
                        song['liked'] = true;
                    } else {
                        song['liked'] = false;
                    }
                    result.push(song);
                }
                this.setState({
                    searchResult: result,
                    loading: false,
                });
            }.bind(this), function (err) {
                console.error(err);
            });
    }

    searchHosts() {
        console.log('searching for hosts!!');
        console.log(this.state.searchText);
    }

    search() {
        if (this.props.searchType == "spotify") {
            this.searchSpotify();
        } else if (this.props.searchType == "hosts") {
            this.searchHosts();
        }
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }
    
    render() {
        let songs = null;        
        if (this.state.searchResult != null && this.state.searchResult.length > 0) {
            songs = this.state.searchResult.map((song) => {
                return <SongChoice selectHost={this.props.selectHost}
                    enqueue={this.props.enqueue}
                    host={this.state.host}
                    dj={this.state.dj}
                    song={song} 
                    like={this.props.like}
                    unlike={this.props.unlike}/>
            });
        }

        const searchIcon = Platform.OS === 'ios'
        ? 'ios-search' : 'md-search';

        const quitIcon = Platform.OS === 'ios'
            ? 'ios-close-circle' : 'md-close-circle';

        const subView = this.state.loading == true ? <View style={styles.subView}><ActivityIndicator color="#E53BF9" size="large" /></View>
            : <View style={styles.subViewContainer}>
                <Ionicons onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                }} size={60} name={quitIcon} style={{ alignSelf: 'center', color: '#E53BF9'}} />
                <ScrollView horizontal={false} >
                    {songs}
                </ScrollView>
                </View>;

        return <View>
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <View style={styles.innerModal}>
                    {subView}
                </View>
            </Modal>
            <Form>
                <View style={styles.form}>
                    <Item floatingLabel
                        style={styles.item}>
                        <Input
                            placeholder=''
                            style={styles.searchInput}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={(text) => this.setState({ searchText: text })}
                        />
                    </Item>
                    <Ionicons onPress={() => { this.search() }} size={50}
                        name={searchIcon} style={styles.searchIcon} />
                </View>
            </Form>
        </View>
    }
}

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({ 
    innerModal: {
        height: deviceHeight * 0.95,
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
    },
    searchIcon: {
        color: '#E53BF9',
        marginTop: 10,
    },
    searchInput: {
        color: '#E53BF9',
        marginLeft: 5,
    },
    form: {
        // flex: 0.8,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'black',
    },
    item: {
        backgroundColor: 'black',
        display: 'flex',
        alignItems: 'center',
        width: deviceWidth * 0.6,
        borderRadius: deviceWidth * 0.05,
        backgroundColor: '#222222',
        color: '#E53BF9',
        borderWidth: 1,
        borderColor: '#E53BF9'
    },
    label: {
        marginLeft: 10,
        color: '#E53BF9'
    },
    subView: {
        marginHorizontal: deviceWidth * 0.025,
        backgroundColor: 'black',
        color: '#E53BF9',
        height: deviceHeight * 0.5,
        flex: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    subViewContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 0.15 * deviceHeight,
    }
});