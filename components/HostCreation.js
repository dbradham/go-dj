import React, { Component } from 'react';
import { View, 
Modal, 
StyleSheet, 
Text, 
Dimensions, 
ImageBackground, 
Platform, 
TouchableOpacity } from 'react-native';
import { Item, Form, Input, Button, Label } from "native-base";
import { Ionicons } from "@expo/vector-icons";
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import CustomCamera from './CustomCamera';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

export default class HostCreation extends Component {
    constructor() {
        super();

        this.state = {
            displayName: '',
            location: '',
            hasCameraPermission: null,
            image: null,
            imageSelectorType: false,
            mode: 'creation',
        }

        this.quit = this.quit.bind(this);
    }

    setImageSelectorTypeVisible(visible) {
        this.setState({ imageSelectorType: visible });
    }

    quit() {
        this.setState({ mode: 'creation' })
    }

    _getCamera = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        if (status === 'granted') {
            console.log('status granted');
            this.setState({ mode: 'camera' });
        }
    }

    _getPhotoLibrary = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status === "granted") {
            let result = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 3]
               });
        if (!result.cancelled) {
            console.log(result);
            console.log('result.uri', result.uri);
            this.setState({ image: result.uri });
        }
            
            
        }
       }

    render() {
        if (this.state.mode == 'camera') {
            return <CustomCamera quit={this.quit} />
        }

        const quitIcon = Platform.OS === 'ios'
        ? 'ios-close-circle' : 'md-close-circle';

        const backIcon = Platform.OS === 'ios'
            ? 'ios-arrow-back' : 'md-arrow-back';
            
            return <ImageBackground
                style={styles.container}
                source={{ uri: 'https://wolfbars.club/wp-content/uploads/2018/05/Untitled-design-67-800x419.png' }}>
                <Ionicons size={35} name={backIcon} style={styles.icon} onPress={this.props.back} />
                <Modal animationType="slide"
                transparent={true}
                visible={this.state.imageSelectorType}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                    <View style={styles.modalView}>
                        <View style={styles.imageUploadOptions}>
                            <TouchableOpacity style={styles.imageUploadOption}
                                onPress={() => this._getCamera()}>
                                <Text style={styles.imageUploadText}>Take a Picture</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.imageUploadOption}
                                onPress={() => this._getPhotoLibrary()}>
                                <Text style={styles.imageUploadText}>Select an Image</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity style={styles.iconContainer}>
                            <Ionicons onPress={() => {
                                    this.setImageSelectorTypeVisible(!this.state.imageSelectorType);
                                }} size={deviceWidth * 0.3} name={quitIcon} style={styles.quitIcon} />
                        </TouchableOpacity>
                    </View>
                </Modal>
                <Form>
                    <View style={styles.form}>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Display Name</Label>
                            <Input
                                style={styles.input}
                                secureTextEntry={false}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={displayName => this.setState({ displayName })}
                            />
                        </Item>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Location</Label>
                            <Input
                                style={styles.input}
                                secureTextEntry={false}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={location => this.setState({ location })}
                            />
                        </Item>
                        <View>
                            <Button full rounded
                                style={styles.signIn}
                                onPress={() => {
                                    this.setImageSelectorTypeVisible(!this.state.imageSelectorType);
                                }}>
                                <Text style={styles.signInText}>Choose an Image</Text>
                            </Button>
                            <Button full rounded
                                style={styles.signIn}
                                onPress={() => this.props.createHost(this.state.displayName, this.state.location, this.state.image)}>
                                <Text style={styles.signInText}>Create Venue</Text>
                            </Button>
                        </View>
                    </View>
                </Form>
            </ImageBackground>;
    }
}
const styles = StyleSheet.create({
    quitIcon: {
        color: '#E53BF9',
    },
    imageUploadText: {
        color: '#E53BF9',
        fontWeight: 'bold',
    },
    imageUploadOption: {
        height: deviceWidth * 0.3,
        width: deviceWidth * 0.3,
        borderRadius: deviceWidth * 0.15,
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageUploadOptions: {
        display: 'flex',
        flexDirection: 'row'
    },
    modalView: {
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: deviceHeight * 0.1,
        // backgroundColor: 'rgba(235, 235, 235, 0.4)',
    },
    icon: {
        marginTop: 30,
        marginLeft: 10,
        color: '#E53BF9',
    },
    input: {
        color: '#E53BF9',
        marginLeft: 5
    },
    container: {
        flex: 1,
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
    },
    item: {
        backgroundColor: 'black',
        display: 'flex',
        alignItems: 'center',
        width: deviceWidth * 0.6,
        borderRadius: deviceWidth * 0.05,
        backgroundColor: '#222222',
        color: '#E53BF9',
        borderWidth: 1,
        borderColor: '#E53BF9'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        height: deviceHeight * 0.7,
        justifyContent: 'center',
        alignItems: 'center',
    },
    signIn: {
        marginTop: 10,
        width: deviceWidth * 0.6,
        backgroundColor: 'transparent',
        borderColor: '#03e8fc',
        borderRadius: 30,
        borderWidth: 1.5,
    },
    signInText: {
        color: '#E53BF9',
        fontWeight: 'bold',
        fontSize: 16
    }
});