import React, { Component } from 'react';
import { Ionicons } from "@expo/vector-icons";
import { Modal, 
View, 
StyleSheet, 
Text, 
Dimensions, 
TouchableOpacity, 
Image, 
Platform } from 'react-native';

class ListedSong extends Component {
    constructor() {
        super();

        this.state = {
            modalVisible: false,
        };
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    readSong(userSelected, album, name) {
        return userSelected == true ?
            <TouchableOpacity style={styles.container} onPress={() => {
                this.setModalVisible(true);
            }}>
                <View style={styles.selectedCountContainer}>
                    <Text style={styles.selectedCount}>{this.props.number}</Text>
                </View>
                <Text style={styles.selectedSongName}>{name}</Text>
            </TouchableOpacity>
            :
            <TouchableOpacity style={styles.container} onPress={() => {
                this.setModalVisible(true);
            }}>
                <View style={styles.countContainer}>
                    <Text style={styles.count}>{this.props.number}</Text>
                </View>
                <Text style={styles.songName}>{name}</Text>
            </TouchableOpacity>
    }
    

    render() {
        const quitIcon = Platform.OS === 'ios'
            ? 'ios-close-circle' : 'md-close-circle';

        const album = this.props.album.length >= 20
            ? this.props.album.substring(0, 19) + '...' : this.props.album;

        let name = this.props.title.replace('feat', 'ft');
        
        name = name.length >= 20 ? name.substring(0, 19) + '...' : name;

        const song = this.readSong(false, album, name);

        return <View>
            <Modal animationType="slide"
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                    Alert.alert('Modal has been closed.');
                }}>
                <View style={styles.innerModel}>
                    <Text style={styles.requestedBy}>{this.props.requestedBy}</Text>
                    <Text style={styles.songNameModal}>{name}</Text>
                    <Image source={{ uri: this.props.src }} style={styles.albumCover} />
                    <Text style={styles.artist}>{this.props.artist}</Text>
                    <Text style={styles.albumName}>{album}</Text>

                    <TouchableOpacity style={styles.iconContainer}>
                        <Ionicons onPress={() => {
                            this.setModalVisible(!this.state.modalVisible);
                        }} size={60} name={quitIcon} style={styles.quitIcon} />
                    </TouchableOpacity>
                </View>
            </Modal>
            {song}
            </View>
    }
}
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        padding: 2.5,
        borderRadius: 5,
        marginBottom: 2.5,
        marginLeft: deviceWidth * 0.1,
    },
    innerModel: {
        marginTop: deviceHeight * 0.1,
        marginLeft: deviceWidth * 0.05,
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#03e8fc',
        height: deviceWidth * 0.8,
        width: deviceWidth * 0.8,
        borderRadius: deviceWidth * 0.45,
    }, 
    quitIcon: {
        color: '#E53BF9',
    },
    iconContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
    albumCover: {
        width: deviceWidth * 0.35,
        height: deviceHeight * 0.175,
        borderWidth: 0.5,
        borderColor: '#03e8fc',
    },
    countContainer: {
        borderRadius: 17.5,
        width: 35,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        borderWidth: 1.5,
        borderColor: '#03e8fc',
        marginRight: 5,
    },
    selectedCountContainer: {
        borderRadius: 17.5,
        width: 35,
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        borderWidth: 1.5,
        borderColor: '#E53BF9',
        marginRight: 5,
    },
    count: {
        color: '#E53BF9',
        padding: 2.5,
        fontSize: 24,
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#03e8fc',
    },
    selectedCount: {
        color: '#03e8fc',
        padding: 2.5,
        fontSize: 24,
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#E53BF9',
    },
    songName: {
        color: '#E53BF9',
        fontSize: 24,
        borderWidth: 1.5,
        borderColor: '#03e8fc',
        paddingHorizontal: 10,
        width: deviceWidth * 0.7,
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#03e8fc',
        borderRadius: 15,
    },
    selectedSongName: {
        color: '#03e8fc',
        fontSize: 24,
        borderWidth: 1.5,
        borderColor: '#E53BF9',
        paddingHorizontal: 10,
        width: deviceWidth * 0.7,
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#E53BF9',
        borderRadius: 15,
    },
    songNameModal: {
        color: '#E53BF9',
        fontSize: 20,
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#03e8fc',
    },
    requestedBy: {
        color: '#E53BF9',
        fontSize: 24,
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#03e8fc',
        marginTop: 5,
    },
    artist: {
        color: '#E53BF9',
        fontSize: 20,
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#03e8fc',
    },
    albumName: {
        color: '#E53BF9',
        fontSize: 16,
        fontStyle: 'italic',
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#03e8fc',
    }
})

export default ListedSong;