import { StyleSheet, Dimensions } from 'react-native';

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        marginVertical: 5,
        paddingVertical: 5,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 5,
        borderWidth: 1,
        borderColor: '#03e8fc',
        // backgroundColor: 'rgba(3, 232, 252, 0.7)', // hex: #03e8fc
        borderRadius: deviceWidth * 0.25,
        width: deviceWidth * 0.9,
    },
    location: {
        color: '#bfbfbf',
        fontSize: 22,
        textShadowRadius: 2.25,
        textShadowOffset: { width: -0.85, height: 0.85 },
        textShadowColor: '#E53BF9',
        fontWeight: 'bold',
    },
    name: {
        color: '#bfbfbf',
        fontSize: 26,
        textShadowRadius: 2.75,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#E53BF9',
        fontWeight: 'bold',
    },
    thumbnail: {
        height: 50,
        width: 50,
        marginRight: 10,
    },
    innerInfo: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    pin: {
        color: 'yellow'
    },
    distance: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    distanceText: {
        color: '#E53BF9',
        fontSize: 18,
    },
    leftSide: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    textContainer: {
        display: 'flex',
        flexDirection: 'column',
        marginLeft: 10,
    },
    image: {
        height: deviceWidth * 0.2,
        width: deviceWidth * 0.2,
        borderRadius: 15,
    }
});
export default styles;