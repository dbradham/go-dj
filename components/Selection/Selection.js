import React, { useState, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, ActivityIndicator } from 'react-native';
import styles from './styles';
import firebase from '../../firebase';

function getHostImage(hostName) {
    const [image, setImage] = useState([]);

    useEffect(() => {
            var storage = firebase.storage();
            var storageRef = storage.ref();
        
            let pathReference = storageRef.child("HostAvatars/" + hostName);
        
            pathReference.getDownloadURL().then((url) => {
                setImage(url);
            })
    });

    return image;
}

const Selection = (props) => {
    const host = props.host;
    const hostName = host.name;
    const hostImage = getHostImage(hostName);

    let img = null;
    if (hostImage.length > 10){
        img = <Image source={{ uri: hostImage }} style={styles.image} />;
    } else {
        img = <ActivityIndicator style={{height: 50, width: 50, color: '#E53BF9' }} />
    }

    return (
        <TouchableOpacity onPress={() => props.onPress(host)}>
            <View style={styles.container}>
                {img}
                <View style={styles.textContainer}>
                    <Text style={styles.name}>{hostName}</Text>
                    <Text style={styles.location}>{host.location}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}
export default Selection;