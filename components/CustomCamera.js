import { Camera } from 'expo-camera';
import {View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import React from 'react';

const CustomCamera = (props) => {
    return <View style={{ flex: 1 }}>
    <Camera style={{ flex: 1 }} type={Camera.Constants.Type.back}>
        <View
        style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
        }}>
        <View style={styles.bottomRow}>
            <TouchableOpacity
                style={{
                flex: 0.1,
                alignSelf: 'flex-end',
                alignItems: 'center',
                }}
                // onPress={() => {
                // setType(
                //     type === Camera.Constants.Type.back
                //     ? Camera.Constants.Type.front
                //     : Camera.Constants.Type.back
                // );}}
                >
                <Text style={styles.text}> Flip </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => props.quit()}
            style={{
                flex: 0.1,
                alignSelf: 'flex-end',
                alignItems: 'center',
                }}>
                    <Text style={styles.text}>Quit</Text>
            </TouchableOpacity>
        </View>
        </View>
    </Camera>
    </View>
}

export default CustomCamera;

const styles = StyleSheet.create({
    bottomRow: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom: 20, 
    },
    text: { 
        fontSize: 18, 
        color: 'white' 
    }
})