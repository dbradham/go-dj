import React, { Component } from 'react';
import { View, Text, Platform, Modal, ActivityIndicator, ScrollView } from 'react-native';
import styles from './styles';
import { Ionicons } from "@expo/vector-icons";
import SongChoice from '../SongChoice';

export default class Header extends Component {
    constructor() {
        super();

        this.state = {
            modalVisible: false,
        }
    }

    displayLikes() {
        if (this.props.role == 'dj') {
            this.setState({
                likedSongs: this.props.dj.likedSongs,
                modalVisible: true
            });
        }
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    render() {
        const backIcon = Platform.OS === 'ios'
            ? 'ios-arrow-back' : 'md-arrow-back';
        
        const planetIcon = Platform.OS === 'ios'
            ? 'ios-heart' : 'md-heart';
        
        const quitIcon = Platform.OS === 'ios'
            ? 'ios-close-circle' : 'md-close-circle';
        
        let likedSongs = null;
        
        if (this.props.role == 'dj' && this.state.likedSongs
            && this.state.likedSongs.length > 0) {
            likedSongs = this.state.likedSongs.map((song) => {
                song['liked'] = true;
                return <SongChoice accessToken={this.props.host.token}
                    selectHost={this.props.selectHost}
                    enqueue={this.props.enqueue}
                    host={this.props.host}
                    dj={this.props.dj}
                    song={song}
                    like={this.props.like}
                    unlike={this.props.unlike}/>
            });
        }

        const subView = <View style={styles.subViewContainer}>
                <Ionicons onPress={() => 
                    this.setModalVisible(!this.state.modalVisible)} 
                    size={60} name={quitIcon} 
                    style={{ color: '#E53BF9' }} />
                <ScrollView horizontal={false} >
                    {likedSongs}
                </ScrollView>
            </View>;
        
        const iconStyle = this.props.showLikes ? 
            styles.icon : styles.iconBlack;

        const text = this.props.text ? this.props.text : "Go DJ";

        return (
            <View>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View style={styles.innerModal}>
                        {subView}
                    </View>
                </Modal>
                <View style={styles.container}>
                    <Ionicons size={35} name={backIcon} style={styles.icon} onPress={this.props.back} />
                    <Text style={styles.header}>{text}</Text>
                    <Ionicons size={35}
                        name={planetIcon}
                        style={iconStyle}
                        onPress={() => this.displayLikes()} />
                </View>
            </View>
        );
    }
}