import { StyleSheet, Dimensions } from 'react-native';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    subView: {
        marginHorizontal: deviceWidth * 0.025,
        backgroundColor: 'black',
        color: '#E53BF9',
        height: deviceHeight * 0.5,
        flex: 0,
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
    },
    subViewContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: deviceHeight * 0.05,
    },
    innerModal: {
        height: deviceHeight * 0.95,
        backgroundColor: 'rgba(0, 0, 0, 0.75)',
    },
    container: {
        marginTop: 25,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        width: deviceWidth,
        backgroundColor: 'black',
        height: 40,
        paddingBottom: 5,
        borderBottomColor: '#03e8fc',
        borderBottomWidth: 1,
    },
    icon: {
        color: '#E53BF9',
        marginLeft: 10,
        marginRight: 10
    },
    iconBlack: {
        color: 'black',
        marginLeft: 10,
        marginRight: 10,
    },
    header: {
        color: '#E53BF9',
        fontSize: 28,
        textShadowRadius: 2.5,
        textShadowOffset: { width: -1, height: 1 },
        textShadowColor: '#03e8fc',
    }
});
export default styles;