import React from 'react';
import { View, Text, Image, TouchableOpacity, Platform } from 'react-native';
import styles from './styles';
import { Ionicons } from "@expo/vector-icons";

const SongChoice = (props) => {
    
    const queue = (song) => {
        const songViewModel = {
            album: song.album,
            name: song.name,
            artists: song.artists,
            uri: song.uri,
            songDuration: song.duration_ms,
            requestedBy: props.dj.name,
        };

        const body = JSON.stringify({
            song: songViewModel,
            host: props.host,
        });
        
        const url = 'http://192.168.50.118:5000/queue';
        
        fetch(url, {
            method: "post",
            headers: {
                "Content-type": "application/json"
            },
            body: body
        })
            .then((res) => res.json())
            .then((result) => {
                console.log('enqueued track!', result);
                props.enqueue(songViewModel);
                props.selectHost(props.host);
            })
            .catch((error) => {
                console.log('failed to queue track');
                console.log(error);
            });
    }

    const play = (uri) => {
        const accessToken = props.host.token;
        
        const body = JSON.stringify({ "uris": [uri] });

        const url = 'https://api.spotify.com/v1/me/player/play';

        fetch(url, {
            method: "put",
            headers: {
                'Authorization': 'Bearer ' + accessToken
            },
            body: body
        })
            .then((res) => res.json())
            .then((result) => {
                // this.selectHost(this.state.host);
            })
            .catch((error) => {
                console.log('failed to play track');
                console.log(error);
            });
    }

    const song = props.song;
    const albumUrl = song.album.images[0].url;

    const playIcon = Platform.OS === 'ios'
        ? 'ios-play' : 'md-play';

    const likeIcon = Platform.OS === 'ios'
        ? 'ios-heart-empty' : 'md-heart-empty';

    const likedIcon = Platform.OS === 'ios'
        ? 'ios-heart' : 'md-heart';

    const heartIcon = song.liked == true ? likedIcon : likeIcon;

    const enqueueIcon = Platform.OS === 'ios'
        ? 'ios-add-circle': 'md-add-circle';

    const playButton = <TouchableOpacity onPress={() => play(song.uri)}>
        <Ionicons name={playIcon} style={styles.playIcon} size={35} />
        </TouchableOpacity>;

    const enqueueButton = <TouchableOpacity onPress={() => queue(song)}>
        <Ionicons name={enqueueIcon} style={styles.playIcon} size={35}/>
        </TouchableOpacity>;

    const isAdmin = false;

    const playOrEnqueueIcon = isAdmin == true ? playButton : enqueueButton;

    const liked = <TouchableOpacity onPress={() => { 
            song.liked=!song.liked; 
            props.unlike(song); 
        }}>
            <Ionicons name={heartIcon} style={styles.playIcon} size={35}/>
        </TouchableOpacity>;

    const unliked = <TouchableOpacity onPress={() => props.like(song)}>
            <Ionicons name={heartIcon} style={styles.playIcon} size={35}/>
        </TouchableOpacity>;

    const likeButton = song.liked == false ? unliked : liked;

    return (
        <TouchableOpacity style={styles.container}>
            <View style={styles.rowContainer}>
                <View style={styles.songSide}>
                    <Image source={{ uri: albumUrl }} style={styles.albumCover} />
                    <View style={styles.nameArtistCol}>
                        <Text style={styles.name}>{song.name}</Text>
                        <Text style={styles.artist}>{song.artists[0].name}</Text>
                        <Text style={styles.album}>{song.album.name}</Text>
                    </View>
                </View>
                <View style={styles.iconSide}>
                    {likeButton}    
                    {playOrEnqueueIcon}
                </View>
            </View>
        </TouchableOpacity>
    );
}
export default SongChoice;