import { StyleSheet, Dimensions } from 'react-native';

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'column',
        borderWidth: 1,
        borderColor: '#03e8fc',
        margin: 5,
        borderRadius: 5,
        backgroundColor: 'black',
    },
    name: {
        color: '#E53BF9',
        fontSize: 20,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        maxWidth: deviceWidth * 0.55,
    },
    artist: {
        color: '#E53BF9',
        fontSize: 16,
    },
    album: {
        color: '#E53BF9',
        fontStyle: 'italic',
        fontSize: 14,
        maxWidth: deviceWidth * 0.55,
    },
    rowContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    nameArtistCol: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
        marginLeft: 10,
        flexWrap: 'wrap',
    },
    playIcon: {
        color: '#E53BF9',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginRight: 10,
    },
    songSide: {
        display: 'flex',
        flexDirection: 'row',
    },
    albumCover: {
        height: 50,
        width: 50,
        marginTop: 10,
        marginLeft: 10,
    },
    iconSide: {
        display: 'flex',
        flexDirection: 'row',
    }
});
export default styles;