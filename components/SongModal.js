// DisplayModal.js

import React, { Component } from 'react'
import { Modal, View, Image, TouchableHighlight, Text, StyleSheet, Dimensions } from 'react-native';

export default class SongModel extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    render() {
        return <View>
        <Modal visible={props.display} animationType="slide"
            onRequestClose={() => console.log('closed')}>
            {/* <Image
                source={props.image}
                style={styles.image} /> */}
            <View style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: '#00000080'
            }}>
                <View style={{
                    width: 300,
                    height: 300,
                    backgroundColor: '#fff',
                    padding: 20
                }}>
                    <Text style={styles.text}>
                        {props.data}
                    </Text>
                </View>
            </View>
        </Modal>
        <TouchableHighlight
            onPress={() => {
                this.setModalVisible(true);
            }}>
            <Text>Show Modal</Text>
        </TouchableHighlight>
    </View>;
    }
}

const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        height: deviceHeight * 0.4
    },
    image: {
        marginTop: 20,
        marginLeft: 90,
        height: 200,
        width: 200
    },
    text: {
        fontSize: 20,
        marginLeft: 150
    }
})

export default SongModal;