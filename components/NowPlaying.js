import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import React from 'react';

const NowPlaying = (props) => {
        const album = props.album.length >= 20
            ? props.album.substring(0, 19) + '...' : props.album;

        let name = props.name.replace('feat', 'ft');
        
        name = name.length >= 20 ? name.substring(0, 19) + '...' : name;

        return <View style={styles.hostContent}>
            <View style={styles.nowPlayingContainer}>
                <View style={styles.nowPlaying}>
                <Text style={styles.songInfo}>{name}</Text>
                    <Image style={styles.nowPlayingCover}
                        source={{ uri: props.coverImage }} />
                <Text style={styles.songInfo}>{props.artist}</Text>
                </View>
            </View>
            
            <View style={styles.queueContainer}>
                {props.queue}
            </View>
        </View>;   
}
export default NowPlaying;

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    queueContainer: {
        display: 'flex',
        alignItems: 'flex-start',
    },
    nowPlayingContainer: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
    },
    nowPlaying: {
        marginVertical: 20,
        width: deviceWidth * 0.85,
        height: deviceWidth * 0.85,
        borderRadius: deviceHeight * 0.2125,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#03e8fc',
        borderWidth: 2,
    },
    songInfo: {
        color: '#E53BF9',
        textShadowRadius: 1,
        textShadowColor: 'red',
        marginHorizontal: 15,
        marginBottom: 5,
        fontSize: 22,
        textShadowRadius: 2.25,
        textShadowOffset: { width: 0.85, height: 0.85 },
        textShadowColor: '#03e8fc',
    },
    album: {
        color: '#E53BF9',
        textShadowRadius: 1,
        textShadowColor: 'red',
        marginHorizontal: 15,
        marginBottom: 5,
        fontSize: 18,
        textShadowRadius: 2.25,
        textShadowOffset: { width: 0.85, height: 0.85 },
        textShadowColor: '#03e8fc',
    },
    nowPlayingCover: {
        height: deviceHeight * 0.2,
        width: deviceWidth * 0.4,
        borderWidth: 0.5,
        borderColor: '#03e8fc',
        marginVertical: 10,
    },
    hostContent: {
        display: 'flex',
        flexDirection: 'column',
    },
    queueText: {
        color: '#E53BF9',
        fontSize: 32,
        marginBottom: 10,
        marginLeft: deviceWidth * 0.05,
        display: 'flex',
        textShadowRadius: 3.5,
        textShadowOffset: { width: 1.35, height: 1.35 },
        textShadowColor: '#03e8fc',
    }
});