import React, { Component } from 'react';
import { View, Text, ImageBackground } from 'react-native';
import styles from './styles';

const HostInfo = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.name}>{props.host.name}</Text>
            <Text style={styles.location}>{props.host.location}</Text>
        </View>
    );
}
export default HostInfo;