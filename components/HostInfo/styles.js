import { StyleSheet, Dimensions } from 'react-native';

const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
    },
    location: {
        color: '#42f5f2'
    },
    name: {
        color: 'white'
    },
});
export default styles;