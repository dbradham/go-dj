const images = {
    bennu: require("./bennu.png"),
    hidden: require("./hidden.png"),
    kungfu: require('./kungfu.png'),
    david: require('./david.png'),
}
export default images;