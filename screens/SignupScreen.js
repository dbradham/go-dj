import React, { Component } from 'react';
import { View, StyleSheet, Text, Dimensions, ImageBackground, Platform } from 'react-native';
import { Item, Form, Input, Button, Label } from "native-base";
import { Ionicons } from "@expo/vector-icons";

export default class SignupScreen extends Component {
    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            cPassword: '',
            role: '',
            displayName: '',
            location: '',
        }
    }

    componentDidMount() {
        this.setState({ role: this.props.role });
    }

    render() {
        const backIcon = Platform.OS === 'ios'
            ? 'ios-arrow-back' : 'md-arrow-back';
        if (this.state.role == 'host') {
            return <ImageBackground
                style={styles.container}
                source={{ uri: 'https://wolfbars.club/wp-content/uploads/2018/05/Untitled-design-67-800x419.png' }}>
                <Ionicons size={35} name={backIcon} style={styles.icon} onPress={this.props.back} />
                <Form>
                    <View style={styles.form}>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Email</Label>
                            <Input
                                style={styles.input}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={email => this.setState({ email })}
                            />
                        </Item>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Password</Label>
                            <Input
                                style={styles.input}
                                secureTextEntry={true}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={password => this.setState({ password })}
                            />
                        </Item>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Confirm Password</Label>
                            <Input
                                style={styles.input}
                                secureTextEntry={true}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={cPassword => this.setState({ cPassword })}
                            />
                        </Item>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Display Name</Label>
                            <Input
                                style={styles.input}
                                secureTextEntry={false}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={displayName => this.setState({ displayName })}
                            />
                        </Item>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Location</Label>
                            <Input
                                style={styles.input}
                                secureTextEntry={false}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={location => this.setState({ location })}
                            />
                        </Item>
                        <View>
                            <Button full rounded
                                style={styles.signIn}
                                onPress={() => this.props.signup(this.state.email, this.state.password, this.state.cPassword, this.state.displayName, this.state.location)}>
                                <Text style={styles.signInText}>Sign Up</Text>
                            </Button>
                        </View>
                    </View>
                </Form>
            </ImageBackground>;
        } else {
            return <ImageBackground
                style={styles.container}
                source={{ uri: 'https://wolfbars.club/wp-content/uploads/2018/05/Untitled-design-67-800x419.png' }}>
                <Ionicons size={35} name={backIcon} style={styles.icon} onPress={this.props.back} />
                <Form>
                    <View style={styles.form}>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Email</Label>
                            <Input
                                style={styles.input}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={email => this.setState({ email })}
                            />
                        </Item>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Password</Label>
                            <Input
                                style={styles.input}
                                secureTextEntry={true}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={password => this.setState({ password })}
                            />
                        </Item>
                        <Item floatingLabel
                            style={styles.item}>
                            <Label style={styles.label}>Confirm Password</Label>
                            <Input
                                style={styles.input}
                                secureTextEntry={true}
                                autoCapitalize="none"
                                autoCorrect={false}
                                onChangeText={cPassword => this.setState({ cPassword })}
                            />
                        </Item>
                        <View>
                            <Button full rounded
                                style={styles.signIn}
                                onPress={() => this.props.signup(this.state.email, this.state.password, this.state.cPassword)}
                            >
                                <Text style={styles.signInText}>Sign Up</Text>
                            </Button>
                        </View>
                    </View>
                </Form>
            </ImageBackground>;
        }
    }
}
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    icon: {
        marginTop: 30,
        marginLeft: 10,
        color: '#E53BF9',
    },
    input: {
        color: '#E53BF9',
        marginLeft: 5
    },
    container: {
        flex: 1,
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
    },
    item: {
        backgroundColor: 'black',
        display: 'flex',
        alignItems: 'center',
        width: deviceWidth * 0.6,
        borderRadius: deviceWidth * 0.05,
        backgroundColor: '#222222',
        color: '#E53BF9',
        borderWidth: 1,
        borderColor: '#E53BF9'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        height: deviceHeight * 0.7,
        justifyContent: 'center',
        alignItems: 'center',
    },
    signIn: {
        marginTop: 10,
        width: deviceWidth * 0.6,
        backgroundColor: 'transparent',
        borderColor: '#03e8fc',
        borderRadius: 30,
        borderWidth: 1.5,
    },
    signInText: {
        color: '#E53BF9',
        fontWeight: 'bold',
        fontSize: 16
    }
});