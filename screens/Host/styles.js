import { StyleSheet, Dimensions } from 'react-native';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    spotify: {
        height: 175,
        width: 175,
        borderRadius: 75,
        alignSelf: 'center'
    },
    spotifySmol: {
        height: 50,
        width: 50,
        alignSelf: 'flex-start',
    },
    header: {
        color: '#E53BF9',
        fontSize: 28,
        marginBottom: 25,
        textShadowRadius: 3.5,
        textShadowOffset: { width: 1.35, height: 1.35 },
        textShadowColor: '#03e8fc',
    },
    contentContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',        
    },
    spotifyContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderWidth: 3.5,
        borderColor: '#03e8fc',
        borderRadius: 120,
        height: 250,
        width: 250,
    },
    spotifyText: {
        color: '#E53BF9',
        fontSize: 22,
        textShadowRadius: 2.25,
        textShadowOffset: { width: 1.25, height: 1.25 },
        textShadowColor: '#03e8fc',
        marginBottom: 20,
        fontWeight: 'bold'
    },
    nowPlaying: {
        borderWidth: 3.5,
        borderColor: '#03e8fc',
        height: deviceHeight * 0.35,
        width: deviceWidth * 0.8,
        borderRadius: deviceHeight * 0.225,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: deviceHeight * 0.075,
    },
    innerInfo: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    songName: {
        marginBottom: 5,
        color: '#E53BF9',
        fontSize: 22,
        textShadowRadius: 2.25,
        textShadowOffset: { width: 1.25, height: 1.25 },
        textShadowColor: '#03e8fc',
        fontWeight: 'bold',
    },
    artist: {
        color: '#E53BF9',
        fontSize: 22,
        textShadowRadius: 2.25,
        textShadowOffset: { width: 1.25, height: 1.25 },
        textShadowColor: '#03e8fc',
        fontWeight: 'bold',
    },
    albumName: {
        color: '#E53BF9',
        fontSize: 22,
        textShadowRadius: 2.25,
        textShadowOffset: { width: 0.85, height: 0.85 },
        textShadowColor: '#03e8fc',
    },
    albumCover: {
        height: deviceWidth * 0.35,
        width: deviceWidth * 0.35,
        borderWidth: 0.75,
        borderColor: '#03e8fc',
    },
    upNext: {
        color: '#E53BF9',
        fontSize: 32,
        marginBottom: 10,
        display: 'flex',
        alignSelf: 'center',
        textShadowRadius: 3.5,
        textShadowOffset: { width: 1.35, height: 1.35 },
        textShadowColor: '#03e8fc',
    },
    upNextContainer: {
        display: 'flex',
        alignItems: 'flex-start',
    },
    topRow: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: deviceWidth * 0.95,
    },
    loadingContainer: {
        backgroundColor: 'black', 
        display: 'flex', 
        flexDirection: 'column', 
        justifyContent: 'center', 
        alignItems: 'center',
        flex: 1,
    }
});
export default styles;