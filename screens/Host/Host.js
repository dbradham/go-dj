import React, { Component } from 'react';
import { View, 
Text, 
ScrollView, 
TouchableOpacity, 
Image, 
ActivityIndicator } from 'react-native';
import styles from './styles';
import Header from '../../components/Header';
import { AuthSession } from 'expo';
import { encode as btoa } from 'base-64';
import ListedSong from '../../components/ListedSong';
import HostCreation from '../../components/HostCreation';
import firebase from '../../firebase';
import 'firebase/storage';
import ToggleSwitch from 'toggle-switch-react-native';
import NowPlaying from '../../components/NowPlaying';

export default class Host extends Component {
    constructor() {
        super();

        this.state = {
            user: {},
            stream: '',
            song: '',
            accessToken: '',
            device: '',
            type: '',
            host: '',
            visible: true,
            loading: false,
        }

        this.authenticate = this.authenticate.bind(this);
        this.setAccessToken = this.setAccessToken.bind(this);
        this.getDbHost = this.getDbHost.bind(this);
        this.createHost = this.createHost.bind(this);
    }

    componentDidMount() {
        if (this.props.host != null) {
            this.setState({ loading: true });
            const host = this.props.host;
            const accessToken = host.token;
            const Spotify = require('spotify-web-api-js');
            let spotifyApi = new Spotify();
            spotifyApi.setAccessToken(accessToken);
            spotifyApi.getMyCurrentPlayingTrack((err, data) => {
                if (err) {
                    console.error(err);
                }
                else {
                    this.setState({ 
                        stream: 'spotify', 
                        accessToken: accessToken,
                        song: data.item,
                        host: host,
                        loading: false,
                    });
                }
            });
        } else {
            this.getDbHost(this.props.user);
        }
    }

    getDbHost(user) {
        const host = firebase
        .firestore()
        .collection('hosts')
        .doc(user.uid);

        host.get().then(function(doc) {
            if (doc.exists) {
                let host = doc.data();
                host.queue = [];
                this.setState({
                    host: host,
                });
                
                this.props.setHostUser(host);
            } else {
                this.setState({
                    host: 'create',
                });
                // doc.data() will be undefined in this case
                console.log("No such document!");
            }
        }.bind(this));
    }

    createHost(name, location, imageUri) {

        this.uploadImage(imageUri, name)
            .then(() => {
                console.log('success uploading image');
            })
            .catch((error) => {
                console.log('error:', error);
            });
        
        const host = {
            name: name,
            location: location,
            public: true,
            visible: true,
            patrons: [],
            queue: [],
        }

        firebase
        .firestore()
        .collection("hosts")
        .doc(this.props.user.uid)
        .set(host)
        .then(function() {
            this.setState({
                host: host
            });
            this.props.setHostUser(host);
        }.bind(this))
        .catch(function(error) {
            console.error("Error writing document: ", error);
        });
    }

    async uploadImage(uri, displayName) {
        const response = await fetch(uri);
        const blob = await response.blob();

        var ref = firebase.storage().ref().child("HostAvatars/" + displayName);
        return ref.put(blob);
    }

    async authenticate() {
        try {
            const spotifyClientId = '272f4705fd4c4d2cb53ff8c16b8cdc3b';
            const spotifyClientSecret = '3e2004dfc3fa4479977949fe25bc9b6b';
            const redirectUrl = 'https://auth.expo.io/@dbradham/godj';
            const scopes = 'user-read-currently-playing user-modify-playback-state user-read-playback-state user-read-private user-read-email streaming app-remote-control';
            const result = await AuthSession.startAsync({
                authUrl:
                    'https://accounts.spotify.com/authorize' +
                    '?response_type=code' +
                    '&client_id=' +
                    spotifyClientId +
                    (scopes ? '&scope=' + encodeURIComponent(scopes) : '') +
                    '&redirect_uri=' +
                    encodeURIComponent(redirectUrl),
            });
            const authCode = result.params.code;
            const credsB64 = btoa(`${spotifyClientId}:${spotifyClientSecret}`);
            const response = await fetch('https://accounts.spotify.com/api/token', {
                method: 'POST',
                headers: {
                    Authorization: `Basic ${credsB64}`,
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                body: `grant_type=authorization_code&code=${authCode}&redirect_uri=${redirectUrl}`,
            });
            const responseJson = await response.json();
            const accessToken = responseJson.access_token;
            this.setAccessToken(accessToken);
            const Spotify = require('spotify-web-api-js');
            let spotifyApi = new Spotify();
            spotifyApi.setAccessToken(accessToken);
            spotifyApi.getMyCurrentPlayingTrack((err, data) => {
                if (err) {
                    console.error(err);
                }
                else {
                    this.setState({ 
                        stream: 'spotify', 
                        accessToken: accessToken,
                        song: data.item,
                    });
                }
            });
        } catch (err) {
            console.error(err)
        }
    }

    setAccessToken(accessToken) {
        firebase
            .firestore()
            .collection('hosts')
            .doc(this.props.user.uid)
            .update({
                token: accessToken
            });
        const host = this.state.host;
        host.token = accessToken;
        this.setState({
            host: host,
            visible: host.visible,
        });
    }

    updateVisibility() {
        firebase
        .firestore()
        .collection('hosts')
        .doc(this.props.user.uid)
        .update({
            visible: !this.state.visible
        });
        this.setState({
            visible: !this.state.visible
        })
    }

    readQueue(queue) {
        if (queue.length == 0){
            return null;
        }
        let count = 0;
        const songs = queue.map((song) => {
            if (song == '') {
                return null;
            }
            count += 1;
            let title = song.name;
            let artist = song.artists[0].name;
            let album = song.album.name;
            let img = song.album.images[0].url;
            let uri = song.uri;
            let requestedBy = song.requestedBy;
            return <ListedSong
                userSelected={false}
                number={count}
                title={title}
                src={img}
                album={album}
                artist={artist}
                uri={uri}
                requestedBy={requestedBy} />
        });
        return <ScrollView>
            {songs}
        </ScrollView>
    }

    render() {
        if (this.state.loading == true) {
            return <View style={styles.loadingContainer}>
                <ActivityIndicator size="large" style={{ color: '#E53BF9' }} />
            </View>;
        }
        if (this.state.host == 'create') {
            return <HostCreation createHost={this.createHost} />
        }

        const src = require('../../client/images/spotify2.png');
        if (this.state.stream == '') {
            return <View style={styles.container}>
                <Header role="host" back={() => this.props.nav("rolepicker")} />
                <View style={styles.contentContainer}>
                    <Text style={styles.header}>Select Your Stream</Text>
                    <TouchableOpacity style={styles.spotifyContainer} onPress={this.authenticate}>
                        <Text></Text>
                        <Image
                            style={styles.spotify}
                            source={src}
                        />
                        <Text style={styles.spotifyText}>Spotify</Text>
                    </TouchableOpacity>
                </View>
            </View>
        } else if (this.state.stream == 'spotify') {
            let queue = null;
            if (this.state.host.queue != '') {
                queue = this.readQueue(this.state.host.queue);
            }
            let artist = '';
            let albumCover = '';
            let album = '';
            let name = '';
            if (this.state.song != '' && this.state.song != undefined) {
                artist = this.state.song.artists[0].name;
                albumCover = this.state.song.album.images[0].url;
                album = this.state.song.album.length >= 19
                    ? this.state.song.album.substring(0, 18) + '...' : this.state.song.album;
                name = this.state.song.name.length >= 19
                    ? this.state.song.name.substring(0, 18) + '...' : this.state.song.name;
            }

            return <View style={styles.container}>
                <Header back={() => this.props.nav("rolepicker")} role="host" />
                <View style={styles.topRow}>
                    <Image style={styles.spotifySmol} source={src}/>
                    <ToggleSwitch
                        isOn={this.state.visible}
                        onColor="green"
                        offColor="grey"
                        size="medium"
                        onToggle={() => this.updateVisibility()}
                        />
                </View>

                <NowPlaying coverImage={albumCover} name={name}
                        artist={artist} album={album} queue={queue} />


            </View>
        } else {
            return null;
        }
    }
}