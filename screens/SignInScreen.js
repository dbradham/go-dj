import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, Platform, Dimensions } from 'react-native';
import { Item, Form, Input, Button, Label } from "native-base";
import SignupScreen from './SignupScreen';

export default class SignInScreen extends Component {
    constructor() {
        super();

        this.state = {
            choice: '',
            role: '',
            email: '',
            password: '',
        }
        this.back = this.back.bind(this);
    }

    back() {
        this.setState({ choice: 'login' });
    }

    render() {
        const baseUri = 'https://wolfbars.club/wp-content/uploads/2018/05/Untitled-design-67-800x419.png'
        const uri = this.props.bg == null ? baseUri : this.props.bg;
        console.log('this.props.bg', this.props);
        if (this.state.choice == 'signup') {
            return <SignupScreen role={this.state.role} signup={this.props.signup} back={this.back} />;
        } else {
            const backIcon = Platform.OS === 'ios'
                ? 'ios-arrow-back' : 'md-arrow-back';
            return <ImageBackground
            style={styles.container}
            source={{ uri: uri }}
        >
            <Form>
                <View style={styles.form}>
                    <Item floatingLabel
                        style={styles.item}>
                        <Label style={styles.label}>Email</Label>
                        <Input
                            style={styles.input}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={email => this.setState({ email })}
                        />
                    </Item>
                    <Item floatingLabel
                        style={styles.item}>
                        <Label style={styles.label}>Password</Label>
                        <Input
                            style={styles.input}
                            secureTextEntry={true}
                            autoCapitalize="none"
                            autoCorrect={false}
                            onChangeText={password => this.setState({ password })}
                        />
                    </Item>
                    <View>
                        <Button full rounded
                            style={styles.signIn}
                            onPress={() => 
                                this.props.login(this.state.email, this.state.password)}
                        >
                            <Text style={styles.signInText}>Login</Text>
                        </Button>
                        <Button full rounded 
                            style={styles.signIn} 
                            onPress={() => this.setState({ choice: 'signup' })} 
                        >
                            <Text style={styles.signInText}>Sign Up</Text>
                    </Button>
                    </View>
                </View>
            </Form>
        </ImageBackground>;
        }
    }
}
const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;
const styles = StyleSheet.create({
    icon: {
        marginTop: 30,
        marginLeft: 10,
        color: '#E53BF9',
    },
    input: {
        color: '#E53BF9',
        marginLeft: 5
    },
    label: {
        marginLeft: 10,
        color: '#E53BF9'
    },
    container: {
        flex: 1,
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
    },
    item: {
        backgroundColor: 'black',
        display: 'flex',
        alignItems: 'center',
        width: deviceWidth * 0.6,
        borderRadius: deviceWidth * 0.05,
        backgroundColor: '#222222',
        color: '#E53BF9',
        borderWidth: 1,
        borderColor: '#E53BF9'
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
        height: deviceHeight * 0.7,
        justifyContent: 'center',
        alignItems: 'center',
    },
    signIn: {
        marginTop: 10,
        width: deviceWidth * 0.6,
        backgroundColor: 'transparent',
        borderColor: '#03e8fc',
        borderRadius: 30,
        borderWidth: 1.5,
    },
    signInText: {
        color: '#E53BF9',
        fontWeight: 'bold',
        fontSize: 16
    }
});