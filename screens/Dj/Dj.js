import React, { Component } from 'react';
import styles from './styles';
import Header from '../../components/Header';
import NowPlaying from '../../components/NowPlaying';
import ListedSong from '../../components/ListedSong';
import Search from '../../components/Search';
import {
    Platform,
    ScrollView,
    Text,
    View,
    Modal,
    Animated,
    Dimensions
} from 'react-native';
import HostSelector from '../../components/HostSelector';
import firebase from '../../firebase';

const deviceHeight = Dimensions.get('window').height;

export default class Dj extends Component {
    constructor() {
        super();

        this.state = {
            user: {},
            modalVisible: false,
            countdown: 5,
            loading: false,
            email: '',
            password: '',
            bounceValue: new Animated.Value(deviceHeight * 0.7),  //This is the initial position of the subview
            isHidden: true,
            likedSongs: [],
            // the content returned by the server for a given search
            searchResult: [],
            // the input text a dj is searching for;
            // updated after every keystroke
            searchText: '',
            // allows us to access API directly from client
            accessToken: '',
            // indicates whether dj should select hosts or songs
            screen: '',
            // currently selected host
            host: {
                name: '',
                location: '',
                queue: []
            },
            dj: {
                email: ''
            },
            // name of the song currently playing at host venue
            song: '',
            // name of the album the current song is off
            album: '',
            // artist that created the current song
            artist: '',
            // album images of the current song
            images: [],
            // name of the device playing the song
            device: '',
            // type of device that is playing the song
            type: '',
        }
        this.back = this.back.bind(this);
        this.setEmail = this.setEmail.bind(this);
        this.setPassword = this.setPassword.bind(this);
        this.getNextSong = this.getNextSong.bind(this);
        this.selectHost = this.selectHost.bind(this);
        this.getDbDj = this.getDbDj.bind(this);
        this.like = this.like.bind(this);
        this.unlike = this.unlike.bind(this);
        this.enqueue = this.enqueue.bind(this);
    }

    componentDidMount() {
        this.setState({ user: this.props.user });
        this.getDbDj(this.props.user);
    }

    getDbDj(user) {
        const dj = firebase
        .firestore()
        .collection('djs')
        .doc(user.uid);

        dj.get().then(function(doc) {
            if (doc.exists) {
                this.setState({
                    dj: doc.data()
                })
            } else {
                firebase
                .firestore()
                .collection("djs")
                .doc(this.props.user.uid)
                .set({
                    likedSongs: []
                })
                .then(function() {
                    this.setState({
                        dj: {
                            likedSongs: []
                        }
                    });
                }.bind(this))
                .catch(function(error) {
                    console.error("Error writing document: ", error);
                });
            }
        }.bind(this));
    }

    setEmail(email) {
        this.setState({ email: email });
    }

    setPassword(password) {
        this.setState({ password: password });
    }

    setDbUser(email) {
        firebase
        .firestore()
        .collection('users')
        .where('email', '==', email)
        .onSnapshot((snapshot) => {
          // todo: fix this so it's not mapping, 
          // just grab the first element of the array
          const user = snapshot.docs[0];
          this.setState({ 
            user: user,
            screen: 'rolepicker',
          });
        });
      }

    selectHost(host) {
        const Spotify = require('spotify-web-api-js');
        let spotifyApi = new Spotify();
        spotifyApi.setAccessToken(host.token);
        spotifyApi.getMyCurrentPlayingTrack(function (err, data) {
            if (err) {
                console.log('error getting curent track!!', err);
                this.setState({ 
                    host: host,
                    song: 'error',
                });
            }
            else {
                this.setState({ 
                    song: data.item,
                    host: host
                });
            }
        }.bind(this));
    }

    enqueue(song) {
        let host = this.state.host;
        let queue = host.queue;
        queue.push(song);
        host.queue = queue;
        this.setState({
            host: host
        });
    }

    getNextSong(host) {
        const remaining = parseInt(this.state.song.item.duration_ms) 
                            - parseInt(this.state.song.progress_ms);
        const interval = remaining < 1500 ? remaining : 1500;
        if (remaining < 15000) {
            this.transition(host);
        } else {
            setTimeout(() => {
                if (this.state.host.name == '') {
                    return;
                } else {
                    this.selectHost(host);
                }
            }, interval);
        }
    }

    transition(host) {
        const songs = this.state.host.queue;
        if (songs[0].requestedBy == this.state.dj.name) {
            // countdown
            
            console.log('transitioning song!!', songs[0]);
            const remaining = parseInt(this.state.song.item.duration_ms) 
                                - parseInt(this.state.song.progress_ms);
            /*
                                let flag = true;
            const timeToStart = remaining - 12000;
            let count = 10;
            setTimeout(() => {
                if (flag == true) {
                    flag = false;
                    const timer = setInterval(() => {
                        if (count <= 0) {
                            this.setState({ modalVisible: false });
                            clearInterval(timer);
                            this.selectHost(host);
                        } else {
                            this.setState({ modalVisible: true, countdown: count });
                            count -= 1;
                        }
                    }, 1000);
                }
            }, timeToStart);
            */
        }
    }

    _toggleSubview() {
        let toValue = deviceHeight * 0.7;
        if (this.state.isHidden) {
            toValue = 0;
        }
        // This will animate the transalteY of the subview between 0 & 100 depending on its current state
        // 100 comes from the style below, which is the height of the subview.
        Animated.spring(
            this.state.bounceValue,
            {
                toValue: toValue,
                velocity: 3,
                tension: 2,
                friction: 8,
            }
        ).start();
        this.state.isHidden = !this.state.isHidden;
    }

    back() {
        this.setState({
            host: {
                name: '',
                location: '',
                queue: [],
            }
        });
    }

    readQueue(queue) {
        if (queue.length == 0){
            return null;
        }
        let count = 0;
        const songs = queue.map((song) => {
            if (song == '') {
                return null;
            }
            count += 1;
            let title = song.name;
            let artist = song.artists[0].name;
            let album = song.album.name;
            let img = song.album.images[0].url;
            let uri = song.uri;
            let requestedBy = song.requestedBy;
            return <ListedSong
                userSelected={false}
                number={count}
                title={title}
                src={img}
                album={album}
                artist={artist}
                uri={uri}
                requestedBy={requestedBy} />
        });
        return <ScrollView>
            {songs}
        </ScrollView>
    }

    like(song) {
        let likedSongs = this.state.dj.likedSongs;
        likedSongs.push(song);
        firebase
            .firestore()
            .collection('djs')
            .doc(this.state.user.uid)
            .update({
                likedSongs: likedSongs
            });
        
        this.setState({
            likedSongs: likedSongs
        });
    }

    unlike(song) {
        let likedSongs = this.state.dj.likedSongs;
        const index = likedSongs.indexOf(song);
        if (index > -1) {
            likedSongs.splice(index, 1);
        }
        firebase
            .firestore()
            .collection('djs')
            .doc(this.state.user.uid)
            .update({
                likedSongs: likedSongs
            });
        
        this.setState({ likedSongs: likedSongs });
    }

    render() {
        if (this.state.host.name == '') {
            return <HostSelector nav={this.props.nav} selectHost={this.selectHost} />
        } else {
            const dj = this.state.dj;
            const accessToken = this.state.host.accessToken;
            const host = this.state.host;
            let name = '';
            let artist = '';
            let album = '';
            let coverImage = '';
            let queue = null;

            if (this.state.song != '' && this.state.song && this.state.song != "error") {
                name = this.state.song.name.split('(')[0];
                queue = this.readQueue(host.queue);
                coverImage = this.state.song.album.images[0].url;
                album = this.state.song.album.name.split('(')[0];
                artist = this.state.song.artists[0].name;
            } else {
                name = 'The Host is Offline';
                // coverImage = require("...logo.png")
            }

            console.log('name', name);

            return <View style={styles.container}>
                <Modal animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View style={styles.innerModal}>
                        <Text style={styles.countdown}
                        >{this.state.countdown}</Text>
                    </View>
                </Modal>
                <View>
                    <Header role="dj"
                        text={host.name}
                        showLikes={true}
                        back={this.back}
                        unlike={this.unlike}
                        like={this.like}
                        host={host}
                        dj={dj}
                        accessToken={accessToken}
                        selectHost={this.selectHost}
                        enqueue={this.enqueue}
                        likedSongs={this.state.likedSongs} />

                    <Search 
                        likedSongs={this.state.likedSongs}
                        like={this.like}
                        unlike={this.unlike} 
                        dj={dj}
                        host={host} 
                        selectHost={this.selectHost}
                        enqueue={this.enqueue}
                        searchType="spotify"
                    />

                    <NowPlaying coverImage={coverImage} name={name}
                        artist={artist} album={album} queue={queue} />

                </View>
            </View>
        }
    }
}