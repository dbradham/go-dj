import { StyleSheet, Dimensions } from 'react-native';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
    },
    header: {
        marginTop: 12,
        alignSelf: 'center',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        color: '#E53BF9',
        fontSize: 32,
        textShadowRadius: 3,
        textShadowOffset: { width: -1.15, height: 1.15 },
        textShadowColor: '#03e8fc',
        fontWeight: 'bold',
    },
    hostsContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 15,
    },
    hostInfo: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        margin: 10,
    },
    hostName: {
        color: '#E53BF9',
        fontSize: 18,
        textShadowRadius: 2.25,
        textShadowOffset: { width: -0.85, height: 0.85 },
        textShadowColor: '#03e8fc',
    },
    hostLocation: {
        color: '#E53BF9',
        fontSize: 18,
        textShadowRadius: 2.25,
        textShadowOffset: { width: -0.85, height: 0.85 },
        textShadowColor: '#03e8fc',
    },
    label: {
        marginLeft: 10,
        color: '#E53BF9'
    },
    innerModal: {
        marginTop: deviceHeight * 0.4,
        marginLeft: deviceWidth * 0.05,
        backgroundColor: 'black',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'transparent',
        //height: deviceHeight * 0.7
        // borderWidth: 2,
        // borderColor: '#03e8fc',
        // height: deviceHeight * 0.45,
        // width: deviceWidth * 0.9,
        // borderRadius: deviceWidth * 0.45,
    },
    countdown: {
        color: '#E53BF9',
        fontSize: 156,
        textShadowRadius: 10,
        textShadowOffset: { width: -5, height: 5 },
        textShadowColor: '#03e8fc',
    }
});
export default styles;