import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        justifyContent: 'space-around',
    },
    headerContainer: {
        display: 'flex',
        alignItems: 'center',
        height: 75,
    },
    header: {
        color: '#E53BF9',
        fontWeight: 'bold',
        fontSize: 44,
        textShadowRadius: 7.5,
        textShadowOffset: { width: 2.5, height: 2.5 },
        textShadowColor: 'black',
    },
    buttons: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'column',
        height: 250
    },
    button: {
        width: 115,
        height: 115,
        borderWidth: 1,
        borderColor: '#03e8fc',
        backgroundColor: 'black',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 15,
    },
    signOutButton: {
        width: 75,
        height: 75,
        borderWidth: 1,
        borderColor: '#03e8fc',
        backgroundColor: 'black',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 35,
        left: 10,
    },
    signOutButtonText:{
        color: '#E53BF9',
        fontSize: 12,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontWeight: 'bold',
    },
    buttonText: {
        color: '#E53BF9',
        fontSize: 22,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        fontWeight: 'bold',
    },
    buttonRow: {
        display: 'flex',
        flexDirection: 'row',
    }
});
export default styles;