import React, { Component } from 'react';
import { View, Text, ImageBackground } from 'react-native';
import { Button } from "native-base";
import styles from './styles';

const RolePicker = (props) => {
    const baseUri = 'https://wolfbars.club/wp-content/uploads/2018/05/Untitled-design-67-800x419.png'
    const uri = props.bg == null ? baseUri : props.bg;

    return <ImageBackground
        source={{ uri: uri }}
        style={styles.container} >
            <Button
                rounded
                style={styles.signOutButton}
                onPress={() => props.signout()}
            ><Text style={styles.signOutButtonText}>Sign Out</Text></Button>
        <View style={styles.headerContainer}>
            <Text style={styles.header}>GO-DJ</Text>
        </View>
        <View style={styles.buttons}>
            <View style={styles.buttonRow}>
                <Button
                    rounded
                    style={styles.button}
                    onPress={() => props.nav("dj")}
                ><Text style={styles.buttonText}>Guests</Text></Button>
                <Button
                    rounded
                    style={styles.button}
                    onPress={() => props.nav("host")}
                ><Text style={styles.buttonText}>Venues</Text></Button>
            </View>
        </View>
    </ImageBackground>
}
export default RolePicker;