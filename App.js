import React, { Component } from 'react';
import Loading from './screens/Loading';
import RolePicker from './screens/RolePicker';
import Dj from './screens/Dj';
import Host from './screens/Host';
import firebase from './firebase';
import SignInScreen from './screens/SignInScreen';
import * as Location from 'expo-location';
import * as Permissions from 'expo-permissions';

import { decode, encode } from 'base-64'
if (!global.btoa) {  global.btoa = encode }
if (!global.atob) { global.atob = decode }

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      screen: 'loading',
      data: '',
      user: null,
      location: null,
      backgroundPhoto: null,
      hostUser: null,
    }

    this.nav = this.nav.bind(this);
    this.login = this.login.bind(this);
    this.setHostUser = this.setHostUser.bind(this);
  }

  componentDidMount() {
    this.checkIfAuthenticated();
    this._getLocationAsync();
  }

  checkIfAuthenticated() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user != null) {
        this.setState({ 
          screen: 'rolepicker', 
          user: user 
        });
      } else {
        this.setState({ screen: 'signin' });
      }
    });
  }

  login(email, password) {
    try {
      firebase.auth().signInWithEmailAndPassword(email, password);
    } catch (error) {
      console.log({
        success: false,
        error: error.message || error
      });
    }
  }
  
  signup(email, password) {
    try {
      firebase.auth().createUserWithEmailAndPassword(email, password);
      firebase.auth().onAuthStateChanged(user => {
        if (user == null) return;

        const updateFirestore = (uid, email) => {
          const users = firebase.firestore().collection('users');
          async function add(uid, email) {
            await users.add({
              uid: uid,
              email: email
            });
          }
          add(uid, email);
        }

        updateFirestore(user.uid, user.email);
      });
    } catch (error) {
      console.log(error.toString(error));
    }
  }

  signout() {
    try {
      firebase.auth().signOut();
    } catch (error) {
      console.log('error signing out', error.message || error);
    }
  }

  nav(screen) {
    this.setState({ screen: screen });
  }

  setDbUser(email) {
    firebase
    .firestore()
    .collection('users')
    .where('email', '==', email)
    .onSnapshot((snapshot) => {
      // todo: fix this so it's not mapping, 
      // just grab the first element of the array
      const user = snapshot.docs[0];
      this.setState({ 
        user: user,
      });
    });
  }

  async _getLocationAsync() {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      console.log('Permission to access location was denied');
    }

    let location = await Location.getCurrentPositionAsync({});
    this.locationPicture(location);
  }

  locationPicture(location) {
    const lat = location.coords.latitude;
    const long = location.coords.longitude;

    const url = 'https://api.teleport.org/api/locations/' + lat + ',' + long + '/';
    fetch(url)
    .then((res) => {
      res.json()
        .then((data) => {
          const baseUrl = data._embedded['location:nearest-urban-areas'][0]._links['location:nearest-urban-area'].href;

          const imagesUrl = baseUrl + 'images/'

          fetch(imagesUrl)
          .then((imgRes) => {
            imgRes.json()
            .then((data) => {
              const img = data.photos[0].image.mobile;

              this.setState({
                location: location,
                backgroundPhoto: img,
              });

              if (this.state.user != null) {
                firebase.firestore()
                .collection("users")
                .doc(this.state.user.uid)
                .update({
                  background: img,
                  location: location,
                });
              }
            })
          })
      });
    })
  }

  setHostUser(host) {
    this.setState({
      hostUser: host
    });
  }

  render() {
    if (this.state.screen == 'dj') {
      return <Dj user={this.state.user} signout={this.signout} nav={this.nav} />;
    } else if (this.state.screen == 'host') {
      return <Host setHostUser={this.setHostUser} host={this.state.hostUser} user={this.state.user} signout={this.signout} nav={this.nav} />;
    } else if (this.state.screen == 'rolepicker') {
      return <RolePicker bg={this.state.backgroundPhoto} signout={this.signout} nav={this.nav} />
    } else if (this.state.screen == 'signin') {
      return <SignInScreen bg={this.state.backgroundPhoto} login={this.login} signup={this.signup} nav={this.nav} />
    } else {
      return <Loading />
    }
  }
}
