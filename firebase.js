import { firebase } from '@firebase/app'
import '@firebase/auth';
import '@firebase/firestore';

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyC5R2AhtS9w2HPcG8RkAS73_MsYHPVeIsE",
    authDomain: "godj-268901.firebaseapp.com",
    databaseURL: "https://godj-268901.firebaseio.com",
    projectId: "godj-268901",
    storageBucket: "godj-268901.appspot.com",
    messagingSenderId: "94902950433",
    appId: "1:94902950433:web:241a8890a9ae7075c5a160",
    measurementId: "G-5SQFQ9R2K0"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;